package com.example.marutiapp.form;

import android.graphics.drawable.Drawable;

public class MenuModel {

    public String menuName;
    public String url;
    public String icon;
    public boolean hasChildren, isGroup;
    private  Drawable drawable;

    public MenuModel(String icon,String menuName, boolean isGroup, boolean hasChildren, String url) {
        this.icon=icon;
        this.menuName = menuName;
        this.url = url;
        this.isGroup = isGroup;
        this.hasChildren = hasChildren;
    }

  /*  public MenuModel(Drawable drawable, String menuName, boolean isGroup, boolean hasChildren, String url) {
        this.drawable=drawable;
        this.menuName = menuName;

        this.isGroup = isGroup;
        this.hasChildren = hasChildren;
        this.url = url;
    }*/
}
