package com.example.marutiapp.form;

public class FmListFom {
    private  String name,number,info,status;

    public FmListFom(String name, String number, String info, String status) {
        this.name = name;
        this.number = number;
        this.info = info;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
