package com.example.marutiapp.form;

public class NewUserModel {
    String FullName,EmpMobileNo,EmpEmailID,EmpRole,StartDate,DealerName,DealerCode,DealerLocation,DealerCity,UserID;

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getEmpMobileNo() {
        return EmpMobileNo;
    }

    public void setEmpMobileNo(String empMobileNo) {
        EmpMobileNo = empMobileNo;
    }

    public String getEmpEmailID() {
        return EmpEmailID;
    }

    public void setEmpEmailID(String empEmailID) {
        EmpEmailID = empEmailID;
    }

    public String getEmpRole() {
        return EmpRole;
    }

    public void setEmpRole(String empRole) {
        EmpRole = empRole;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getDealerName() {
        return DealerName;
    }

    public void setDealerName(String dealerName) {
        DealerName = dealerName;
    }

    public String getDealerCode() {
        return DealerCode;
    }

    public void setDealerCode(String dealerCode) {
        DealerCode = dealerCode;
    }

    public String getDealerLocation() {
        return DealerLocation;
    }

    public void setDealerLocation(String dealerLocation) {
        DealerLocation = dealerLocation;
    }

    public String getDealerCity() {
        return DealerCity;
    }

    public void setDealerCity(String dealerCity) {
        DealerCity = dealerCity;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public NewUserModel() {
    }

    public NewUserModel(String fullName, String empMobileNo, String empEmailID, String empRole, String startDate, String dealerName, String dealerCode, String dealerLocation, String dealerCity, String userID) {
        FullName = fullName;
        EmpMobileNo = empMobileNo;
        EmpEmailID = empEmailID;
        EmpRole = empRole;
        StartDate = startDate;
        DealerName = dealerName;
        DealerCode = dealerCode;
        DealerLocation = dealerLocation;
        DealerCity = dealerCity;
        UserID = userID;
    }
}
