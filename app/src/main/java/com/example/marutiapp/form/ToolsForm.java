package com.example.marutiapp.form;

public class ToolsForm {
    private  String tvToolName;

    public ToolsForm(String tvToolName) {
        this.tvToolName = tvToolName;
    }

    public String getTvToolName() {
        return tvToolName;
    }

    public void setTvToolName(String tvToolName) {
        this.tvToolName = tvToolName;
    }
}
