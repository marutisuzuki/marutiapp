package com.example.marutiapp.form;

public class ExcelListModel {
    String ExcelName,ExcelID;
int Excelimg;
    public String getExcelID() {
        return ExcelID;
    }

    public void setExcelID(String excelID) {
        ExcelID = excelID;
    }

    public int getExcelimg() {
        return Excelimg;
    }

    public void setExcelimg(int excelimg) {
        Excelimg = excelimg;
    }

    public String getExcelName() {
        return ExcelName;
    }

    public void setExcelName(String excelName) {
        ExcelName = excelName;
    }

    public ExcelListModel(String ExcelId,int excelimg, String excelName) {
        ExcelID=ExcelId;
        Excelimg = excelimg;
        ExcelName = excelName;
    }

    public ExcelListModel() {
    }
}
