package com.example.marutiapp.form;

public class AvailableStockModel {
    String  StockId,StockName,Stockavailable,StockAmt;

    public String getStockId() {
        return StockId;
    }

    public void setStockId(String stockId) {
        StockId = stockId;
    }

    public String getStockName() {
        return StockName;
    }

    public void setStockName(String stockName) {
        StockName = stockName;
    }

    public String getStockavailable() {
        return Stockavailable;
    }

    public void setStockavailable(String stockavailable) {
        Stockavailable = stockavailable;
    }

    public String getStockAmt() {
        return StockAmt;
    }

    public void setStockAmt(String stockAmt) {
        StockAmt = stockAmt;
    }

    public AvailableStockModel() {
    }

    public AvailableStockModel(String stockId, String stockName, String stockavailable, String stockAmt) {
        StockId = stockId;
        StockName = stockName;
        Stockavailable = stockavailable;
        StockAmt = stockAmt;
    }
}
