package com.example.marutiapp.form;

import java.util.ArrayList;

public class Model_country {
    String str_country;
    ArrayList al_state;

    String str_name;
    String str_description;

    public String getStr_name() {
        return str_name;
    }

    public void setStr_name(String str_name) {
        this.str_name = str_name;
    }

    public String getStr_description() {
        return str_description;
    }

    public void setStr_description(String str_description) {
        this.str_description = str_description;
    }

    public String getStr_image() {
        return str_image;
    }

    public void setStr_image(String str_image) {
        this.str_image = str_image;
    }

    String str_image;

    public Model_country() {
    }

    public Model_country(String str_country, ArrayList al_state) {
        this.str_country = str_country;
        this.al_state = al_state;
    }

    public Model_country(String str_country, ArrayList al_state, String str_name, String str_description, String str_image) {
        this.str_country = str_country;
        this.al_state = al_state;
        this.str_name = str_name;
        this.str_description = str_description;
        this.str_image = str_image;
    }

    public String getStr_country() {
        return str_country;
    }

    public void setStr_country(String str_country) {
        this.str_country = str_country;
    }

    public ArrayList getAl_state() {
        return al_state;
    }

    public void setAl_state(ArrayList al_state) {
        this.al_state = al_state;
    }
}