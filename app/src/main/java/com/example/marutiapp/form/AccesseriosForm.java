package com.example.marutiapp.form;

public class AccesseriosForm {
    private  String AccName,AccQty,AccPrice;

    public AccesseriosForm(String accName, String accQty, String accPrice) {
        AccName = accName;
        AccQty = accQty;
        AccPrice = accPrice;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getAccQty() {
        return AccQty;
    }

    public void setAccQty(String accQty) {
        AccQty = accQty;
    }

    public String getAccPrice() {
        return AccPrice;
    }

    public void setAccPrice(String accPrice) {
        AccPrice = accPrice;
    }
}
