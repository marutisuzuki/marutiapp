package com.example.marutiapp.form;

public class LeaveStatusModel {
    String LeavestatusID,LeaveEmpID,LeaveEmpName,LeavefromDate,LeaveTodate,LeaveReason,LeaveStatus;

    public String getLeavestatusID() {
        return LeavestatusID;
    }

    public void setLeavestatusID(String leavestatusID) {
        LeavestatusID = leavestatusID;
    }

    public String getLeaveEmpID() {
        return LeaveEmpID;
    }

    public void setLeaveEmpID(String leaveEmpID) {
        LeaveEmpID = leaveEmpID;
    }

    public String getLeaveEmpName() {
        return LeaveEmpName;
    }

    public void setLeaveEmpName(String leaveEmpName) {
        LeaveEmpName = leaveEmpName;
    }

    public String getLeavefromDate() {
        return LeavefromDate;
    }

    public void setLeavefromDate(String leavefromDate) {
        LeavefromDate = leavefromDate;
    }

    public String getLeaveTodate() {
        return LeaveTodate;
    }

    public void setLeaveTodate(String leaveTodate) {
        LeaveTodate = leaveTodate;
    }

    public String getLeaveReason() {
        return LeaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        LeaveReason = leaveReason;
    }

    public String getLeaveStatus() {
        return LeaveStatus;
    }

    public void setLeaveStatus(String leaveStatus) {
        LeaveStatus = leaveStatus;
    }

    public LeaveStatusModel() {
    }

    public LeaveStatusModel(String leavestatusID, String leaveEmpID, String leaveEmpName, String leavefromDate, String leaveTodate, String leaveReason, String leaveStatus) {
        LeavestatusID = leavestatusID;
        LeaveEmpID = leaveEmpID;
        LeaveEmpName = leaveEmpName;
        LeavefromDate = leavefromDate;
        LeaveTodate = leaveTodate;
        LeaveReason = leaveReason;
        LeaveStatus = leaveStatus;
    }
}
