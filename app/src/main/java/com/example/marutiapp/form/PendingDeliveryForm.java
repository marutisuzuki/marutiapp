package com.example.marutiapp.form;

public class PendingDeliveryForm {
    private  String custDetails,bookingnNumber,custAddres,vehDetail,exeDetail,requestedDate, preChecklistStatus;

    public String getCustDetails() {
        return custDetails;
    }

    public PendingDeliveryForm(String custDetails, String bookingnNumber, String custAddres, String vehDetail, String exeDetail, String requestedDate, String preXhecklistStatus) {
        this.custDetails = custDetails;
        this.bookingnNumber = bookingnNumber;
        this.custAddres = custAddres;
        this.vehDetail = vehDetail;
        this.exeDetail = exeDetail;
        this.requestedDate = requestedDate;
        this.preChecklistStatus = preXhecklistStatus;
    }

    public PendingDeliveryForm() {
    }

    public void setCustDetails(String custDetails) {
        this.custDetails = custDetails;
    }

    public String getBookingnNumber() {
        return bookingnNumber;
    }

    public void setBookingnNumber(String bookingnNumber) {
        this.bookingnNumber = bookingnNumber;
    }

    public String getCustAddres() {
        return custAddres;
    }

    public void setCustAddres(String custAddres) {
        this.custAddres = custAddres;
    }

    public String getVehDetail() {
        return vehDetail;
    }

    public void setVehDetail(String vehDetail) {
        this.vehDetail = vehDetail;
    }

    public String getExeDetail() {
        return exeDetail;
    }

    public void setExeDetail(String exeDetail) {
        this.exeDetail = exeDetail;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getPreChecklistStatus() {
        return preChecklistStatus;
    }

    public void setPreChecklistStatus(String preChecklistStatus) {
        this.preChecklistStatus = preChecklistStatus;
    }
}
