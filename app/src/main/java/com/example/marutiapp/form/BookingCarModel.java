package com.example.marutiapp.form;

public class BookingCarModel {
    String DateTime,bookingcarid,bookingcarpersonname,bookingdesc,bookingstatus;

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getBookingcarid() {
        return bookingcarid;
    }

    public void setBookingcarid(String bookingcarid) {
        this.bookingcarid = bookingcarid;
    }

    public String getBookingcarpersonname() {
        return bookingcarpersonname;
    }

    public void setBookingcarpersonname(String bookingcarpersonname) {
        this.bookingcarpersonname = bookingcarpersonname;
    }

    public String getBookingdesc() {
        return bookingdesc;
    }

    public void setBookingdesc(String bookingdesc) {
        this.bookingdesc = bookingdesc;
    }

    public String getBookingstatus() {
        return bookingstatus;
    }

    public void setBookingstatus(String bookingstatus) {
        this.bookingstatus = bookingstatus;
    }

    public BookingCarModel() {
    }

    public BookingCarModel(String dateTime, String bookingcarid, String bookingcarpersonname, String bookingdesc, String bookingstatus) {
        DateTime = dateTime;
        this.bookingcarid = bookingcarid;
        this.bookingcarpersonname = bookingcarpersonname;
        this.bookingdesc = bookingdesc;
        this.bookingstatus = bookingstatus;
    }
}
