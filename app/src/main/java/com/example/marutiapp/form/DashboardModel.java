package com.example.marutiapp.form;

public class DashboardModel {
    String ItemID,ItemTitle,ItemDesc,ItemName,ItemCount;

    public String getItemTitle() {
        return ItemTitle;
    }

    public void setItemTitle(String itemTitle) {
        ItemTitle = itemTitle;
    }

    public String getItemDesc() {
        return ItemDesc;
    }

    public void setItemDesc(String itemDesc) {
        ItemDesc = itemDesc;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemCount() {
        return ItemCount;
    }

    public void setItemCount(String itemCount) {
        ItemCount = itemCount;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public DashboardModel() {
    }

    public DashboardModel(String itemID, String itemTitle, String itemDesc, String itemName, String itemCount) {
        ItemID = itemID;
        ItemTitle = itemTitle;
        ItemDesc = itemDesc;
        ItemName = itemName;
        ItemCount = itemCount;
    }
}
