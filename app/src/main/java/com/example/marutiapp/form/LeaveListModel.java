package com.example.marutiapp.form;

public class LeaveListModel {
    String LeaveId,LeaveDate,LeaveFull,LeaveHalf;

    public String getLeaveId() {
        return LeaveId;
    }

    public void setLeaveId(String leaveId) {
        LeaveId = leaveId;
    }

    public String getLeaveDate() {
        return LeaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        LeaveDate = leaveDate;
    }

    public String getLeaveFull() {
        return LeaveFull;
    }

    public void setLeaveFull(String leaveFull) {
        LeaveFull = leaveFull;
    }

    public String getLeaveHalf() {
        return LeaveHalf;
    }

    public void setLeaveHalf(String leaveHalf) {
        LeaveHalf = leaveHalf;
    }

    public LeaveListModel() {
    }

    public LeaveListModel(String leaveId, String leaveDate, String leaveFull, String leaveHalf) {
        LeaveId = leaveId;
        LeaveDate = leaveDate;
        LeaveFull = leaveFull;
        LeaveHalf = leaveHalf;
    }
}
