package com.example.marutiapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.DashboardModel;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder>{
    private DashboardModel[] listdata;
    public DashboardAdapter(DashboardModel[] listdata1) {
        this.listdata = listdata1;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_dashboard1, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DashboardModel myListData = listdata[position];
        holder.tv_itemTitle.setText(listdata[position].getItemTitle());
        holder.tv_itemDesc.setText(listdata[position].getItemDesc());
        holder.tv_itemName.setText(listdata[position].getItemName());
        holder.tv_itemCount.setText(listdata[position].getItemCount());
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_itemCount,tv_itemName,tv_itemDesc,tv_itemTitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
           tv_itemTitle = itemView.findViewById(R.id.tv_itemTitle);
           tv_itemDesc = itemView.findViewById(R.id.tv_itemDesc);
            tv_itemName = itemView.findViewById(R.id.tv_itemName);
            tv_itemCount = itemView.findViewById(R.id.tv_itemCount);
        }
    }
}