package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityACMApprovalDetails;
import com.example.marutiapp.activity.ActivityALMApprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVALMListAdapter extends RecyclerView.Adapter<RVALMListAdapter.ItemViewHolder> {
    private Context mContext;
    private  ArrayList<FmListFom> arrayListALM;


    public RVALMListAdapter(Context mContext, ArrayList<FmListFom> almListFomArrayList) {
        this.mContext=mContext;
        this.arrayListALM =almListFomArrayList;

    }

    @NonNull
    @Override
    public RVALMListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVALMListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arrayListALM.get(position).getName());
        holder.tvNumber.setText(arrayListALM.get(position).getNumber());
        holder.tvStatus.setText(arrayListALM.get(position).getStatus());
        if (arrayListALM.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arrayListALM.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arrayListALM.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);


            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityALMApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}
