package com.example.marutiapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.marutiapp.fragment.FragmentChangePassword;
import com.example.marutiapp.fragment.FragmentPreACM;
import com.example.marutiapp.fragment.FragmentPreDC;
import com.example.marutiapp.fragment.FragmentPreFM;
import com.example.marutiapp.fragment.FragmentPreMGAM;
import com.example.marutiapp.fragment.FragmentPrePDI;
import com.example.marutiapp.fragment.FragmentPreRTO;
import com.example.marutiapp.fragment.FragmentProfile;

public class ViewPagerAdapterPreliminary extends FragmentPagerAdapter {

    public ViewPagerAdapterPreliminary(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentPreACM();
        }
        else if (position == 1)
        {
            fragment = new FragmentPreFM();
        }
        else if (position == 2)
        {
            fragment = new FragmentPreMGAM();
        }
        else if (position == 3)
        {
            fragment = new FragmentPreDC();
        }

        else if (position == 4)
        {
            fragment = new FragmentPreRTO();
        }

        else if (position == 5)
        {
            fragment = new FragmentPrePDI();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "ACM";
        }
        else if (position == 1)
        {
            title = "FM";
        }
        else if (position == 2)
        {
            title = "MGAM";
        }
        else if (position == 3)
        {
            title = "DC";
        }
        else if (position == 4)
        {
            title = "RTO";
        }
        else if (position == 5)
        {
            title = "PDI";
        }

        return title;
    }
}
