package com.example.marutiapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityDeliveryMemo;
import com.example.marutiapp.form.ToolsForm;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class RVToolsDetailsAdapter  extends RecyclerView.Adapter<RVToolsDetailsAdapter.ItemViewHolder> {
    private  Context mContext;
    private  ArrayList<ToolsForm> toolsFormArrayList;

    public RVToolsDetailsAdapter(Context mContext, ArrayList<ToolsForm> toolsFormArrayList) {
        this.mContext=mContext;
        this.toolsFormArrayList=toolsFormArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_tool_details, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvToolName.setText(toolsFormArrayList.get(position).getTvToolName());

    }

    @Override
    public int getItemCount() {
        return toolsFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvToolName;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvToolName=itemView.findViewById(R.id.tv_tool_name);
        }
    }
}
