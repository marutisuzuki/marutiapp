package com.example.marutiapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.marutiapp.fragment.FragmentAvailaleStock;
import com.example.marutiapp.fragment.FragmentUploadStockInfo;

public class ViewPagerAdapterUploadStock  extends FragmentPagerAdapter {

    public ViewPagerAdapterUploadStock(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentUploadStockInfo();
        }
        else if (position == 1)
        {
            fragment = new FragmentAvailaleStock();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Upload Stock";
        }
        else if (position == 1)
        {
            title = "Available Stock";
        }

        return title;
    }
}


