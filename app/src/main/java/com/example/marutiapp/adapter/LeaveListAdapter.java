package com.example.marutiapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.LeaveListModel;


public class LeaveListAdapter extends RecyclerView.Adapter<LeaveListAdapter.ViewHolder>{
    private LeaveListModel[] listdata;
    public LeaveListAdapter(LeaveListModel[] listdata1) {
        this.listdata = listdata1;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_leave_datelist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final LeaveListModel myListData = listdata[position];
        holder.tv_leavedate.setText(listdata[position].getLeaveDate());

    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_leavedate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_leavedate = itemView.findViewById(R.id.tv_leavedate);

        }
    }
}
