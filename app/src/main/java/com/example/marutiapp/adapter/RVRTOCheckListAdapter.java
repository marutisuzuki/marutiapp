package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityMGAMChecklistApprovalDetails;
import com.example.marutiapp.activity.ActivityRTOCheckApproval;
import com.example.marutiapp.activity.ActivityRTOChecklistApprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVRTOCheckListAdapter extends RecyclerView.Adapter<RVRTOCheckListAdapter.ItemViewHolder> {
    private  Context mContext;
    private  ArrayList<FmListFom> arrayListRTOChecklist;

    public RVRTOCheckListAdapter(Context mContext, ArrayList<FmListFom> rtoCheckListFomArrayList) {
        this.mContext=mContext;
        this.arrayListRTOChecklist=rtoCheckListFomArrayList;
    }

    @NonNull
    @Override
    public RVRTOCheckListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVRTOCheckListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arrayListRTOChecklist.get(position).getName());
        holder.tvNumber.setText(arrayListRTOChecklist.get(position).getNumber());
        holder.tvStatus.setText(arrayListRTOChecklist.get(position).getStatus());
        if (arrayListRTOChecklist.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arrayListRTOChecklist.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arrayListRTOChecklist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityRTOChecklistApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}
