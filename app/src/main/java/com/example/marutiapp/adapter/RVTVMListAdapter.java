package com.example.marutiapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.form.FmListFom;
import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityTVMApprovalDetails;

import java.util.ArrayList;

public class RVTVMListAdapter extends RecyclerView.Adapter<RVTVMListAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<FmListFom> arraylistTVM;
    public RVTVMListAdapter(Context mCntext, ArrayList<FmListFom> fmListFomArrayList) {
        this.mContext=mCntext;
        this.arraylistTVM=fmListFomArrayList;

    }

    @NonNull
    @Override
    public RVTVMListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull RVTVMListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arraylistTVM.get(position).getName());
        holder.tvNumber.setText(arraylistTVM.get(position).getNumber());
        holder.tvStatus.setText(arraylistTVM.get(position).getStatus());
        if (arraylistTVM.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }

        holder.tvInfo.setText(arraylistTVM.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arraylistTVM.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityTVMApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}
