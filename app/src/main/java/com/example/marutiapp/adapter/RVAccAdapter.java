package com.example.marutiapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityPriceDetails;
import com.example.marutiapp.form.AccesseriosForm;

import java.util.ArrayList;

public class RVAccAdapter extends RecyclerView.Adapter<RVAccAdapter.ItemViewHolder> {
    private Context mContext;
    private  ArrayList<AccesseriosForm> accesseriosFormArrayList;

    public RVAccAdapter(Context mContext, ArrayList<AccesseriosForm> accesseriosFormArrayList) {
        this.mContext=mContext;
        this.accesseriosFormArrayList=accesseriosFormArrayList;
    }

    @NonNull
    @Override
    public RVAccAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rv_acc_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVAccAdapter.ItemViewHolder holder, int position) {
        holder.tvAccName.setText(accesseriosFormArrayList.get(position).getAccName());
        holder.tvAccQty.setText(accesseriosFormArrayList.get(position).getAccQty());
        holder.tvAccPrice.setText(accesseriosFormArrayList.get(position).getAccPrice());

    }

    @Override
    public int getItemCount() {
        return accesseriosFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAccName,tvAccQty,tvAccPrice;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAccName=itemView.findViewById(R.id.tv_acc_name);
            tvAccQty=itemView.findViewById(R.id.tv_acc_qty);
            tvAccPrice=itemView.findViewById(R.id.tv_acc_price);
        }
    }
}
