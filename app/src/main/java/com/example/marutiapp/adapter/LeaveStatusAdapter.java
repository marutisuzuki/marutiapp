package com.example.marutiapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.LeaveStatusModel;


public class LeaveStatusAdapter extends RecyclerView.Adapter<LeaveStatusAdapter.ViewHolder>{
    private LeaveStatusModel[] listdata;
    public LeaveStatusAdapter(LeaveStatusModel[] listdata1) {
        this.listdata = listdata1;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_leave_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final LeaveStatusModel myListData = listdata[position];

        holder.emp_IDLeave.setText(listdata[position].getLeaveEmpID());
        holder.emp_NameLeave.setText(listdata[position].getLeaveEmpName());
        holder.tv_Leavedatefrom.setText(listdata[position].getLeavefromDate());
        holder.tv_Leavedateto.setText(listdata[position].getLeaveTodate());
        holder.tv_LeaveReason.setText(listdata[position].getLeaveReason());
        holder.tv_leaveStatus.setText(listdata[position].getLeaveStatus());
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView emp_IDLeave,emp_NameLeave,tv_Leavedatefrom,tv_Leavedateto,tv_LeaveReason,tv_leaveStatus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_leaveStatus = itemView.findViewById(R.id.tv_leaveStatus);
            tv_LeaveReason = itemView.findViewById(R.id.tv_LeaveReason);
            tv_Leavedateto = itemView.findViewById(R.id.tv_Leavedateto);
            tv_Leavedatefrom = itemView.findViewById(R.id.tv_Leavedatefrom);
            emp_NameLeave = itemView.findViewById(R.id.emp_NameLeave);
            emp_IDLeave = itemView.findViewById(R.id.emp_IDLeave);

        }
    }
}
