package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityFMPreCheckApprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVFMPreCheckListAdapter extends RecyclerView.Adapter<RVFMPreCheckListAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<FmListFom> arraylistFMChecklist;

    public RVFMPreCheckListAdapter(Context mContext, ArrayList<FmListFom> fmCheckListFomArrayList) {
        this.mContext=mContext;
        this.arraylistFMChecklist=fmCheckListFomArrayList;
    }

    @NonNull
    @Override
    public RVFMPreCheckListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVFMPreCheckListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arraylistFMChecklist.get(position).getName());
        holder.tvNumber.setText(arraylistFMChecklist.get(position).getNumber());
        holder.tvStatus.setText(arraylistFMChecklist.get(position).getStatus());
        if (arraylistFMChecklist.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arraylistFMChecklist.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arraylistFMChecklist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                  //  Button button = (Button) view.findViewById(R.id.dialog_ok);

                   AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                   // builder.setTitle("Name");
                    // set the custom layout

                    final View customLayout = LayoutInflater.from(mContext).inflate(R.layout.dailog_customer_prefered, null);
                  //  final View customLayout = mContext.getLayoutInflater().inflate(R.layout.dailog_customer_prefered, null);
                    builder.setView(customLayout);
                    ImageView ivSal=customLayout.findViewById(R.id.iv_sal_icon);
                    ImageView ivSelf=customLayout.findViewById(R.id.iv_self);
                    Button btnOk=customLayout.findViewById(R.id.btn_ok);
                    ivSal.setVisibility(View.VISIBLE);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(mContext, ActivityFMPreCheckApprovalDetails.class);
                            mContext.startActivity(intent);

                        }
                    });



                    // create and show the alert dialog
                    AlertDialog dialog = builder.create();
                    dialog.show();


                }
            });
        }
    }
}

