package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityMGAMChecklistApprovalDetails;
import com.example.marutiapp.activity.ActivityPDICheckApproval;
import com.example.marutiapp.activity.ActivityPDIChecklistApprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVPDICheckListAdapter extends RecyclerView.Adapter<RVPDICheckListAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<FmListFom> arrayListPDIChecklist;

    public RVPDICheckListAdapter(Context mContext, ArrayList<FmListFom> pdiCheckListFomArrayList) {
        this.mContext=mContext;
        this.arrayListPDIChecklist=pdiCheckListFomArrayList;
    }

    @NonNull
    @Override
    public RVPDICheckListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVPDICheckListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arrayListPDIChecklist.get(position).getName());
        holder.tvNumber.setText(arrayListPDIChecklist.get(position).getNumber());
        holder.tvStatus.setText(arrayListPDIChecklist.get(position).getStatus());
        if (arrayListPDIChecklist.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arrayListPDIChecklist.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arrayListPDIChecklist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityPDIChecklistApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}
