package com.example.marutiapp.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.ExcelListModel;

public class DisplayExcelListAdapter extends RecyclerView.Adapter<DisplayExcelListAdapter.ViewHolder>{
    private ExcelListModel[] listdata;
    public DisplayExcelListAdapter(ExcelListModel[] listdata1) {
        this.listdata = listdata1;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_excel_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ExcelListModel myListData = listdata[position];
        holder.textView.setText(listdata[position].getExcelName());
        holder.imageView.setImageResource(Integer.parseInt(String.valueOf(listdata[position].getExcelimg())));


        /*holder.img_cancel.setOnClickListener(View.OnClickListener){

        }*/
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,img_cancel;
        public TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_excelsample);
            img_cancel = itemView.findViewById(R.id.img_cancel);
          textView = itemView.findViewById(R.id.tv_excelname);
        }
    }
}





