package com.example.marutiapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.AvailableStockModel;

public class AvailableStockAdapter extends RecyclerView.Adapter<AvailableStockAdapter.ViewHolder>{
    private AvailableStockModel[] listdata;
    public AvailableStockAdapter(AvailableStockModel[] listdata1) {
        this.listdata = listdata1;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.activity_available_stock_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AvailableStockModel myListData = listdata[position];
        holder.StockName.setText(listdata[position].getStockName());
        holder.Stockavailable.setText(listdata[position].getStockavailable());
        holder.StockCount.setText(listdata[position].getStockAmt());
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView StockName,Stockavailable,StockCount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            StockName = itemView.findViewById(R.id.StockName);
            Stockavailable = itemView.findViewById(R.id.Stockavailable);
            StockCount = itemView.findViewById(R.id.StockCount);
        }
    }
}
