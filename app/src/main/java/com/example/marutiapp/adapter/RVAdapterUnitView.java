package com.example.marutiapp.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.DeleteListener;
import com.example.marutiapp.form.VoucherInfo;


import java.util.ArrayList;

public class RVAdapterUnitView extends RecyclerView.Adapter<RVAdapterUnitView.MyHolder> {
  private Context mContext;
  private  ArrayList<VoucherInfo> arrayListUnit;
  private DeleteListener deleteListener;


    public RVAdapterUnitView(Context applicationContext, ArrayList<VoucherInfo> arrayListUnit, DeleteListener deleteListener) {
    this.mContext=applicationContext;
    this.arrayListUnit=arrayListUnit;
    this.deleteListener=deleteListener;

    }

    @NonNull
    @Override
    public RVAdapterUnitView.MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_unit_item, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapterUnitView.MyHolder myHolder, final int i) {
        myHolder.tvUnitname.setText(arrayListUnit.get(i).getName());
        String price= String.valueOf(arrayListUnit.get(i).getPrice());
        myHolder.tvUnitPrice.setText(price);
        myHolder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               deleteListener.getDeleteListenerPosition(i);
               notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayListUnit.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private  TextView tvUnitname,tvUnitPrice;
        private ImageButton btnCancel;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            tvUnitname=itemView.findViewById(R.id.tv_unit_name);
            tvUnitPrice=itemView.findViewById(R.id.tv_unit_price);
            btnCancel=itemView.findViewById(R.id.img_btn_cancel);
        }
    }
}
