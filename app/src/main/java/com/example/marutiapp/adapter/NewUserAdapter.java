package com.example.marutiapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.NewUserModel;


public class NewUserAdapter extends RecyclerView.Adapter<NewUserAdapter.ViewHolder> {
    private NewUserModel[] listdata;

    public NewUserAdapter(NewUserModel[] listdata1) {
        this.listdata = listdata1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.activity_new_user_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final NewUserModel myListData = listdata[position];
        holder.tv_startDate.setText(listdata[position].getStartDate());
        holder.tv_DealerRole.setText(listdata[position].getEmpRole());
        holder.emp_Dealerlocation.setText(listdata[position].getDealerLocation());
        holder.emp_DealerName.setText(listdata[position].getDealerName());
        holder.emp_EmailID.setText(listdata[position].getEmpEmailID());
        holder.emp_mobileNo.setText(listdata[position].getEmpMobileNo());
        holder.emp_FullName.setText(listdata[position].getFullName());
        holder.tv_Dealer_code.setText(listdata[position].getDealerCode());
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_Dealer_code, emp_FullName, emp_mobileNo, emp_EmailID, emp_DealerName, emp_Dealerlocation, tv_DealerRole, tv_startDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_startDate = itemView.findViewById(R.id.tv_startDate);
            tv_DealerRole = itemView.findViewById(R.id.tv_DealerRole);
            emp_Dealerlocation = itemView.findViewById(R.id.emp_Dealerlocation);
            emp_DealerName = itemView.findViewById(R.id.emp_DealerName);
            emp_EmailID = itemView.findViewById(R.id.emp_EmailID);
            emp_mobileNo = itemView.findViewById(R.id.emp_mobileNo);
            emp_FullName = itemView.findViewById(R.id.emp_FullName);
            tv_Dealer_code = itemView.findViewById(R.id.tv_Dealer_code);
        }

    }
}
