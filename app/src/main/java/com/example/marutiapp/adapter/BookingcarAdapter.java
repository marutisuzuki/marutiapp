package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.form.BookingCarModel;
import com.example.marutiapp.activity.BookingFollowupActivity;


public class BookingcarAdapter extends RecyclerView.Adapter<BookingcarAdapter.ViewHolder> {
    private BookingCarModel[] listdata;

    public BookingcarAdapter(BookingCarModel[] listdata1) {
        this.listdata = listdata1;
    }

    private Context context;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.activity_bookingforcar, parent, false);
        context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    public BookingcarAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BookingCarModel myListData = listdata[position];

        holder.tv_personName.setText(listdata[position].getBookingcarpersonname());
        holder.tv_dateTime.setText(listdata[position].getDateTime());
        holder.tv_bookingdesc.setText(listdata[position].getBookingdesc());
        holder.tv_bookingStatus.setText(listdata[position].getBookingstatus());
        holder.img_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, BookingFollowupActivity.class);
                context.startActivity(in);
            }
        });
        /*holder.img_cancel.setOnClickListener(View.OnClickListener){

        }*/
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_bookingStatus, tv_bookingdesc, tv_dateTime, tv_personName;
        ImageView img_next;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_bookingdesc = itemView.findViewById(R.id.tv_bookingdesc);
            tv_dateTime = itemView.findViewById(R.id.tv_dateTime);
            tv_personName = itemView.findViewById(R.id.tv_personName);
            tv_bookingStatus = itemView.findViewById(R.id.tv_bookingStatus);
            img_next = itemView.findViewById(R.id.img_next);

        }


    }
}
