package com.example.marutiapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.marutiapp.fragment.FragmentCheckACM;
import com.example.marutiapp.fragment.FragmentCheckDC;
import com.example.marutiapp.fragment.FragmentCheckFM;
import com.example.marutiapp.fragment.FragmentCheckMGAM;
import com.example.marutiapp.fragment.FragmentCheckPDI;
import com.example.marutiapp.fragment.FragmentCheckRTO;

public class ViewPagerAdapterCheckList extends FragmentPagerAdapter {

    public ViewPagerAdapterCheckList(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentCheckACM();
        }
        else if (position == 1)
        {
            fragment = new FragmentCheckFM();
        }
        else if (position == 2)
        {
            fragment = new FragmentCheckMGAM();
        }
        else if (position == 3)
        {
            fragment = new FragmentCheckDC();
        }

        else if (position == 4)
        {
            fragment = new FragmentCheckRTO();
        }

        else if (position == 5)
        {
            fragment = new FragmentCheckPDI();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "ACM";
        }
        else if (position == 1)
        {
            title = "FM";
        }
        else if (position == 2)
        {
            title = "MGAM";
        }
        else if (position == 3)
        {
            title = "DC";
        }
        else if (position == 4)
        {
            title = "RTO";
        }
        else if (position == 5)
        {
            title = "PDI";
        }

        return title;
    }
}
