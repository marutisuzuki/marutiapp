package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityFMCheckApproval;
import com.example.marutiapp.activity.ActivityFMCheckApprovalDetails;
import com.example.marutiapp.activity.ActivityFMPreCheckApprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVFMCheckListAdapter  extends RecyclerView.Adapter<RVFMCheckListAdapter.ItemViewHolder> {
    private Context mContext;
    private  ArrayList<FmListFom> arrayListFMChecklist;

    public RVFMCheckListAdapter(Context mContext, ArrayList<FmListFom> fmCheckListFomArrayList) {
        this.mContext=mContext;
        this.arrayListFMChecklist=fmCheckListFomArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvName.setText(arrayListFMChecklist.get(position).getName());
        holder.tvNumber.setText(arrayListFMChecklist.get(position).getNumber());
        holder.tvStatus.setText(arrayListFMChecklist.get(position).getStatus());
        if (arrayListFMChecklist.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arrayListFMChecklist.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return arrayListFMChecklist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    //  Button button = (Button) view.findViewById(R.id.dialog_ok);

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    // builder.setTitle("Name");
                    // set the custom layout

                    final View customLayout = LayoutInflater.from(mContext).inflate(R.layout.dailog_customer_prefered, null);
                    //  final View customLayout = mContext.getLayoutInflater().inflate(R.layout.dailog_customer_prefered, null);
                    builder.setView(customLayout);
                    ImageView ivSal=customLayout.findViewById(R.id.iv_sal_icon);
                    ImageView ivSelf=customLayout.findViewById(R.id.iv_self);
                    Button btnOk=customLayout.findViewById(R.id.btn_ok);
                    ivSal.setVisibility(View.VISIBLE);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(mContext, ActivityFMCheckApprovalDetails.class);
                            mContext.startActivity(intent);

                        }
                    });



                    // create and show the alert dialog
                    AlertDialog dialog = builder.create();
                    dialog.show();


                }
            });
        }
    }
}

