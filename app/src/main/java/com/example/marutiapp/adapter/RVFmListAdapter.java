package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.form.FmListFom;
import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityFMApprovalDetails;

import java.util.ArrayList;

public class RVFmListAdapter extends RecyclerView.Adapter<RVFmListAdapter.ItemViewHolder> {
    private Context mContext;
    private  ArrayList<FmListFom> fmListFomArrayList;

    public RVFmListAdapter(Context context, ArrayList<FmListFom> fmListFomArrayListinfo) {
        this.mContext=context;
        this.fmListFomArrayList=fmListFomArrayListinfo;

        //for(int i=0; i<fmListFomArrayList.get)
    }

    @NonNull
    @Override
    public RVFmListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVFmListAdapter.ItemViewHolder holder, int position) {

       // Toast.makeText(mContext, ""+fmListFomArrayList.get(position).getName(), Toast.LENGTH_SHORT).show();
        holder.tvName.setText(fmListFomArrayList.get(position).getName());
        holder.tvNumber.setText(fmListFomArrayList.get(position).getNumber());
        holder.tvStatus.setText(fmListFomArrayList.get(position).getStatus());

        if (fmListFomArrayList.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }

        holder.tvInfo.setText(fmListFomArrayList.get(position).getInfo());


    }

    @Override
    public int getItemCount() {
        return fmListFomArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityFMApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}
