package com.example.marutiapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.marutiapp.fragment.FragmentACM;
import com.example.marutiapp.fragment.FragmentFM;
import com.example.marutiapp.fragment.FragmentTVM;

public class ViewPagerAdapterPendingRFCs extends FragmentPagerAdapter {

    public ViewPagerAdapterPendingRFCs(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentFM();
        }
        else if (position == 1)
        {
            fragment = new FragmentACM();
        }
        else if (position == 2)
        {
            fragment = new FragmentTVM();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "FM";
        }
        else if (position == 1)
        {
            title = "ACM";
        }
        else if (position == 2)
        {
            title = "TVM";
        }


        return title;
    }
}

