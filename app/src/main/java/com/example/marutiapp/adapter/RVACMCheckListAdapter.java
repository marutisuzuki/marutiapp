package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityACMApprovalDetails;
import com.example.marutiapp.activity.ActivityACMCheckApproval;
import com.example.marutiapp.activity.ActivityACMCheckApprovalDetails;
import com.example.marutiapp.activity.ActivityRTOpprovalDetails;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class RVACMCheckListAdapter extends RecyclerView.Adapter<RVACMCheckListAdapter.ItemViewHolder> {
    private  Context mContext;
    private  ArrayList<FmListFom> arrayListACMChecklist;


    public RVACMCheckListAdapter(Context mContext, ArrayList<FmListFom> acmCheckListFomArrayList) {
        this.mContext=mContext;
        this.arrayListACMChecklist=acmCheckListFomArrayList;

    }

    @NonNull
    @Override
    public RVACMCheckListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fm_approval_list, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVACMCheckListAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(arrayListACMChecklist.get(position).getName());
        holder.tvNumber.setText(arrayListACMChecklist.get(position).getNumber());
        holder.tvStatus.setText(arrayListACMChecklist.get(position).getStatus());
        if (arrayListACMChecklist.get(position).getStatus()=="Pending")
        {
            holder.tvStatus.setTextColor(Color.parseColor("#F44336"));
        }
        else
        {
            holder.tvStatus.setTextColor(Color.parseColor("#00cc00"));
        }
        holder.tvInfo.setText(arrayListACMChecklist.get(position).getInfo());

    }

    @Override
    public int getItemCount() {
        return arrayListACMChecklist.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvNumber,tvStatus,tvInfo;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvNumber=itemView.findViewById(R.id.tv_number);
            tvStatus=itemView.findViewById(R.id.tv_status);
            tvInfo=itemView.findViewById(R.id.tv_info);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, ActivityACMCheckApprovalDetails.class);
                    mContext.startActivity(intent);

                }
            });
        }
    }
}

