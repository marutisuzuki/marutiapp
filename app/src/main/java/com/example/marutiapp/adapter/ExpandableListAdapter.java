package com.example.marutiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityGatePass;
import com.example.marutiapp.activity.ActivityPendingAllotment;
import com.example.marutiapp.activity.ActivityPriceDetails;
import com.example.marutiapp.activity.ActivityUploadStock;
import com.example.marutiapp.activity.AvailableStockActivity;
import com.example.marutiapp.activity.DashboardActivity;
import com.example.marutiapp.activity.LeaveApplicationActivity;
import com.example.marutiapp.activity.NewUserListActivity;
import com.example.marutiapp.form.MenuModel;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<MenuModel> listDataHeader;
    private HashMap<MenuModel, List<MenuModel>> listDataChild;

    public ExpandableListAdapter(Context context, List<MenuModel> listDataHeader,
                                 HashMap<MenuModel, List<MenuModel>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public MenuModel getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).menuName;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_child, null);
        }

        TextView txtListChild = convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);

        //upload stock
        if (groupPosition == 1) {
            /* if (childText.equals("Stock Upload")) {*/
            if (childPosition == 0) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityUploadStock.class);
                        context.startActivity(intent);
                    }
                });

            } /*else if (childText.equals("Available Stock")) {*/ else if (childPosition == 1) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, AvailableStockActivity.class);
                        context.startActivity(intent);
                    }
                });

            }
        } else if (groupPosition == 2) {

        /*    *//* if (childText.equals("FM")) {*//*
            if (childPosition == 0) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityACM.class);
                        context.startActivity(intent);
                    }
                });

            }*//* else if (childText.equals("TVM")) {*//* else if (childPosition == 1) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityFM.class);
                        context.startActivity(intent);
                    }
                });
            } *//*else if (childText.equals("ACM")) {*//* else if (childPosition == 2) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityTVM.class);
                        context.startActivity(intent);
                    }
                });
            }*/
        } /*else if (groupPosition == 4) {
            if (childPosition == 0) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityACMPCheck.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 1) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityFMChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 2) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityMGAM.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 3) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityDCPreChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 4) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityRTO.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 5) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityPDI.class);
                        context.startActivity(intent);
                    }
                });
            }
        } */else if (groupPosition == 5) {
      /*      if (childPosition == 0) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityACMCheckList.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 1) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityFMChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 2) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityMGAMChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 3) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityDCChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 4) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityRTOChecklist.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 5) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ActivityPDIChecklist.class);
                        context.startActivity(intent);
                    }
                });
            }
        } else if (groupPosition == 10) {

            if (childPosition == 0) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ProfileActivity.class);
                        context.startActivity(intent);
                    }
                });
            } else if (childPosition == 1) {
                txtListChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ChangeActivity.class);
                        context.startActivity(intent);
                    }
                });
            }*/
        }




      /*  //Salestrac
        else if(childText.equals("Booking Car"))
        {
            txtListChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BookingCarActivity.class);
                    context.startActivity(intent);
                }
            });

        }
        else if(childText.equals("Request For Car"))
        {
            txtListChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RequestForCarActivity.class);
                    context.startActivity(intent);
                }
            });

        }
        else if(childText.equals("Cancelled Booking"))
        {
            txtListChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BookingCancelledActivity.class);
                    context.startActivity(intent);
                }
            });

        }*/
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (this.listDataChild.get(this.listDataHeader.get(groupPosition)) == null)
            return 0;
        else
            return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                    .size();
    }

    @Override
    public MenuModel getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).menuName;
        //  Drawable icon=getGroup(groupPosition).icon;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_header, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        ImageView imageView = convertView.findViewById(R.id.icon);

        //  Picasso.with(context).load(String.valueOf(getGroup(groupPosition).icon)).into(imageView);

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        if /*(headerTitle.equals("Pending Allotment")) {*/ (groupPosition == 0) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DashboardActivity.class);
                    context.startActivity(intent);
                }
            });

        } else if /*(headerTitle.equals("Dashboard"))*/ (groupPosition == 3) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActivityPendingAllotment.class);
                    context.startActivity(intent);
                }
            });

        } else if /*(headerTitle.equals("Leave Application"))*/ (groupPosition == 6) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LeaveApplicationActivity.class);
                    context.startActivity(intent);
                }
            });

        } else if /*(headerTitle.equals("Gate Pass"))*/ (groupPosition == 7) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActivityGatePass.class);
                    context.startActivity(intent);
                }
            });

        } else if /*(headerTitle.equals("Price Details"))*/ (groupPosition == 8) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActivityPriceDetails.class);
                    context.startActivity(intent);
                }
            });

        } else if /*(headerTitle.equals("Uesr Management"))*/ (groupPosition == 9) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUser = new Intent(context, NewUserListActivity.class);
                    context.startActivity(intentUser);
                }
            });

        } else if /*(headerTitle.equals("Uesr Management"))*/ (groupPosition == 11) {
            lblListHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();

                }
            });
            //Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();

        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}