package com.example.marutiapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;


public class NewUserActivity extends AppCompatActivity {
    Button btn_saveregistration,btn_cancelregistration;
    ImageView img_listdetails;
    EditText et_firstName,et_middleName,et_lastName;
    EditText et_empDealerID,et_mobileNo,et_emailID,et_date;
    TextView tv_DealerCode,tv_DealerName,tv_DealerCity,tv_locationDesc;
    Spinner sp_role;
    String[] arraySpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_creation);
        this.setTitle("Add Creation");
        init();
        initListener();
    }

    private void initListener() {
        btn_saveregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validation()) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(NewUserActivity.this);
                    dialog.setTitle("User Management");
                    dialog.setMessage("User Save Successfully");
                    dialog.setCancelable(true);
                    dialog.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int
                                        id) {
                                    Toast.makeText(NewUserActivity.this, "User Save Successfully..", Toast.LENGTH_SHORT).show();
                                    // Continue with some operation
                                    dialog.dismiss();

                                }
                            });

                    AlertDialog alert = dialog.create();
                    alert.show();
                }
            }
        });


        img_listdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewUserActivity.this, NewUserListActivity.class);
                startActivity(intent);
            }

        });
    }

    private boolean Validation() {
        if (et_firstName.getText().toString().equalsIgnoreCase("")) {
            et_firstName.setError("Enter Name");
            return false;
        }
        if (et_middleName.getText().toString().equalsIgnoreCase("")) {
            et_middleName.setError("Enter MiddleName");
            return false;
        }if (et_lastName.getText().toString().equalsIgnoreCase("")) {
            et_lastName.setError("Enter LastName");
            return false;
        }

        if (et_empDealerID.getText().toString().equalsIgnoreCase("")) {
            et_empDealerID.setError("Enter Emp");
            return false;
        }

        if (et_mobileNo.getText().toString().equalsIgnoreCase("")) {
            et_mobileNo.setError("Enter Mobile No.");
            return false;
        }

        if (et_emailID.getText().toString().equalsIgnoreCase("")) {
            et_emailID.setError("Enter Email ID");
            return false;
        }
        if (et_date.getText().toString().equalsIgnoreCase("")) {
            et_date.setError("Enter Date");
            return false;
        }

        if (sp_role.getSelectedItem().toString().trim().equalsIgnoreCase("Select Role")) {
            Toast.makeText(NewUserActivity.this, "Select Role", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void init() {
        btn_saveregistration=findViewById(R.id.btn_saveregistration);
        img_listdetails=findViewById(R.id.img_listdetails);
        btn_cancelregistration=findViewById(R.id.btn_cancelregistration);
        et_firstName=findViewById(R.id.et_firstName);
        et_middleName=findViewById(R.id.et_middleName);
        et_lastName=findViewById(R.id.et_lastName);
        et_empDealerID=findViewById(R.id.et_empDealerID);
        et_mobileNo=findViewById(R.id.et_mobileNo);
        et_emailID=findViewById(R.id.et_emailID);
        et_date=findViewById(R.id.et_date);
        tv_DealerCode=findViewById(R.id.tv_DealerCode);
        tv_DealerName=findViewById(R.id.tv_DealerName);
        tv_DealerCity=findViewById(R.id.tv_DealerCity);
        tv_locationDesc=findViewById(R.id.tv_locationDesc);
        sp_role=findViewById(R.id.sp_role);

        arraySpinner = new String[]{
                "Select Role", "Manager", "Accountant","GM"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_role.setAdapter(adapter);
    }
}
