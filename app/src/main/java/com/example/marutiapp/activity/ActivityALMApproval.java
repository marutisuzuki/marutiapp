package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVACMListAdapter;
import com.example.marutiapp.adapter.RVALMListAdapter;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class ActivityALMApproval extends AppCompatActivity {

    private RecyclerView rvALMApproval;

    private ArrayList<FmListFom> almListFomArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_approval);

        init();

        //  fmListFomArrayList.clear();
        almListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
        //  fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        almListFomArrayList.add(new FmListFom("M/S RAVIKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
      //  acmListFomArrayList.add(new FmListFom("M/S YASHRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
       // acmListFomArrayList.add(new FmListFom("M/S ROOPESH TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        //     fmListFomArrayList.add(new FmListFom("M/S RAJKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (almListFomArrayList != null && almListFomArrayList.size() > 0) {
            rvALMApproval.setVisibility(View.GONE);
            rvALMApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvALMApproval.setLayoutManager(linearLayoutManager);
            RVALMListAdapter acmlist = new RVALMListAdapter(ActivityALMApproval.this, almListFomArrayList);

            rvALMApproval.setHasFixedSize(true);
            rvALMApproval.setNestedScrollingEnabled(false);

            rvALMApproval.setAdapter(acmlist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvALMApproval =findViewById(R.id.rv_fm_list);
        almListFomArrayList =new ArrayList<>();

    }
}



