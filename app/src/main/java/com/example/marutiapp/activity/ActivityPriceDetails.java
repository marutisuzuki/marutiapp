package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVAccAdapter;
import com.example.marutiapp.form.AccesseriosForm;

import java.util.ArrayList;

public class ActivityPriceDetails extends AppCompatActivity {
    private RecyclerView rvAccDetails;
    private ArrayList<AccesseriosForm> accesseriosFormArrayList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_price_details);
        init();

      //  accesseriosFormArrayList.clear();

        accesseriosFormArrayList.add(new AccesseriosForm("Car Covers","1","100"));
        accesseriosFormArrayList.add(new AccesseriosForm("Booster Cable","1","200"));
        accesseriosFormArrayList.add(new AccesseriosForm("Car Mats","1","50"));
        accesseriosFormArrayList.add(new AccesseriosForm("Seat Covers","1","70"));
        accesseriosFormArrayList.add(new AccesseriosForm("Cleaning Equipment","1","100"));
        if (accesseriosFormArrayList != null && accesseriosFormArrayList.size() > 0) {
            rvAccDetails.setVisibility(View.GONE);
            rvAccDetails.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvAccDetails.setLayoutManager(linearLayoutManager);
            RVAccAdapter acclist = new RVAccAdapter(ActivityPriceDetails.this, accesseriosFormArrayList);

            rvAccDetails.setHasFixedSize(true);
            rvAccDetails.setNestedScrollingEnabled(false);

            rvAccDetails.setAdapter(acclist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvAccDetails=findViewById(R.id.rv_acc_details);
        accesseriosFormArrayList=new ArrayList<>();
    }
}
