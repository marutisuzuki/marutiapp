package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.form.FmListFom;
import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVTVMListAdapter;

import java.util.ArrayList;

public  class ActivityTVMApproval extends AppCompatActivity {

    private RecyclerView rvTVMApproval;

    private ArrayList<FmListFom> fmListFomArrayList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvm_approval);
        init();

        //  fmListFomArrayList.clear();
        fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
     //   fmListFomArrayList.add(new FmListFom("M/S RAVIKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
    //    fmListFomArrayList.add(new FmListFom("M/S YASHRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
     //   fmListFomArrayList.add(new FmListFom("M/S ROOPESH TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

   //     fmListFomArrayList.add(new FmListFom("M/S RAJKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (fmListFomArrayList != null && fmListFomArrayList.size() > 0) {
            rvTVMApproval.setVisibility(View.GONE);
            rvTVMApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvTVMApproval.setLayoutManager(linearLayoutManager);
            RVTVMListAdapter rvtvmListAdapter = new RVTVMListAdapter(ActivityTVMApproval.this, fmListFomArrayList);

            rvTVMApproval.setHasFixedSize(true);
            rvTVMApproval.setNestedScrollingEnabled(false);

            rvTVMApproval.setAdapter(rvtvmListAdapter);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvTVMApproval =findViewById(R.id.rv_tvm_list);
        fmListFomArrayList=new ArrayList<>();

    }
}
