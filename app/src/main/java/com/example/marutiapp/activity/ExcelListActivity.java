package com.example.marutiapp.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.DisplayExcelListAdapter;
import com.example.marutiapp.form.ExcelListModel;

public class ExcelListActivity extends AppCompatActivity {

   public ExcelListModel[] myListData1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_excel_list);
        myListData1 = new ExcelListModel[]{


                new ExcelListModel("", R.drawable.stockuploadimg1, "Sample1.xls"),
                new ExcelListModel("", R.drawable.stockuploadimg1, "Sample2.xls"),
                new ExcelListModel("", R.drawable.stockuploadimg1, "Sample3.xls"),
                new ExcelListModel("", R.drawable.stockuploadimg1, "Sample4.xls"),
        };
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_excel_list);
        DisplayExcelListAdapter adapter = new DisplayExcelListAdapter(myListData1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }
}