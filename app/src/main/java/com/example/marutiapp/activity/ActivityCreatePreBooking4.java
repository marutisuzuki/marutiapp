package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreatePreBooking4 extends AppCompatActivity {
    Button saveandcontinue4;
    Context context;
    RadioButton rb_extendedyes,rb_extendedno,rb_loyalityyes,rb_loyalityno,rb_insuranceyes,rb_insuranceno,rb_MCPyes,rb_MCPno,rb_accessoriesyes,rb_accessoriesno;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking4);
        context=this;
        init();
        initListner();
    }
    private void initListner() {
        saveandcontinue4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ActivityCreateBooking5.class);
                startActivity(intent);
            }
        });
    }
    private void init() {
        saveandcontinue4=findViewById(R.id.saveandcontinue4);
        rb_extendedyes=findViewById(R.id.rb_extendedyes);
        rb_extendedno=findViewById(R.id.rb_extendedno);
        rb_loyalityyes=findViewById(R.id.rb_loyalityyes);
        rb_loyalityno=findViewById(R.id.rb_loyalityno);
        rb_insuranceyes=findViewById(R.id.rb_insuranceyes);
        rb_insuranceno=findViewById(R.id.rb_insuranceno);
        rb_MCPyes=findViewById(R.id.rb_MCPyes);
        rb_MCPno=findViewById(R.id.rb_MCPno);
        rb_accessoriesyes=findViewById(R.id.rb_accessoriesyes);
        rb_accessoriesno=findViewById(R.id.rb_accessoriesno);
    }
}
