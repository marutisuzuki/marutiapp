package com.example.marutiapp.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.marutiapp.R;
import com.example.marutiapp.adapter.LeaveListAdapter;
import com.example.marutiapp.form.LeaveListModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class LeaveApplicationActivity extends AppCompatActivity {
    Context context;
    public LeaveListModel[] myListData1;
    TextView deliveryReqdate, tv_empName, tv_empFromdate, tv_empTodate;
    Button btn_initiateRFC, btn_save, btn_viewApproval,btn_attachletter;
    EditText et_reason;
    RecyclerView recyclerView;
     Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave_emp);
        this.setTitle("Leave Application");
        context=this;
        init();
        initListener();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new LeaveListModel[]{
                new LeaveListModel("", "23/04/2020", "", ""),
                new LeaveListModel("", "27/07/2020", "", ""),
        };
        LeaveListAdapter adapter = new LeaveListAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initListener() {
        tv_empFromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(LeaveApplicationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tv_empTodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(LeaveApplicationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_viewApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,LeaveStatusActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
         recyclerView = findViewById(R.id.rv_leaveList);
        deliveryReqdate =findViewById(R.id.deliveryReqdate);
        tv_empName =findViewById(R.id.tv_empName);
        tv_empFromdate =findViewById(R.id.tv_empFromdate);
        tv_empTodate =findViewById(R.id.tv_empTodate);
        btn_initiateRFC =findViewById(R.id.btn_initiateRFC);
        btn_save =findViewById(R.id.btn_save);
        et_reason =findViewById(R.id.et_reason);
        btn_viewApproval =findViewById(R.id.btn_viewApproval);
        btn_attachletter =findViewById(R.id.btn_attachletter);
        myCalendar = Calendar.getInstance();

      date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tv_empFromdate.setText(sdf.format(myCalendar.getTime()));
    }
}
