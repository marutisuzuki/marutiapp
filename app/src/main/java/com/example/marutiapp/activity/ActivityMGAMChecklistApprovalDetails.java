package com.example.marutiapp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityMGAMChecklistApprovalDetails extends AppCompatActivity {
    private RadioGroup rgToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mgam_checklist_approval_detail);
        init();

        rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:

                        break;

                    case R.id.rd_search:

                        AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityMGAMChecklistApprovalDetails.this);
                        dialog.setTitle("Action Confirmation");
                        dialog.setMessage("Are you sure to want to Approve?");
                        dialog.setCancelable(true);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                        dialog.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int
                                            id) {

                                   //     Toast.makeText(ActivityMGAMApprovalDetails.this, "Approved Successfully..", Toast.LENGTH_SHORT).show();
                                        // Continue with some operation
                                    //    dialog.dismiss();

                                    }
                                });

// A null listener allows the button to dismiss the dialog and take no further action.

                        dialog.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert = dialog.create();
                        alert.show();


                        break;
                }
            }
        });


    }

    private void init() {
        rgToggle = findViewById(R.id.rgToggle);
    }
}



