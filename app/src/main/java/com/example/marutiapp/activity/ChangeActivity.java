package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;


public class ChangeActivity extends AppCompatActivity {
    Button btn_changepass;
    TextView et_confirmedpass,et_newpass,et_oldpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        this.setTitle("Change Password");
        init();
        initlistener();

    }

    private void initlistener() {
        btn_changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validation()){
                    Toast.makeText(ChangeActivity.this,"Password Changed Successfully",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean validation() {
        if(et_oldpass.getText().toString().equalsIgnoreCase("")){
            et_oldpass.setError("Enter Old Password");
            return false;

        }
        if(et_newpass.getText().toString().equalsIgnoreCase("")){
            et_newpass.setError("Enter New Password");
            return false;

        }
        if(et_confirmedpass.getText().toString().equalsIgnoreCase("")){
            et_confirmedpass.setError("Enter Confirmed Password");
            return false;

        }


        return true;
    }

    private void init() {
        et_oldpass=findViewById(R.id.et_oldpass);
        et_newpass=findViewById(R.id.et_newpass);
        et_confirmedpass=findViewById(R.id.et_confirmedpass);
        btn_changepass=findViewById(R.id.btn_changepass);
    }
}
