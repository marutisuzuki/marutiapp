package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreateBooking6 extends AppCompatActivity {
    Button saveandcontinue6;
    Context context;
    Spinner sp_financeorcash,sp_finance;
    EditText et_financeDiscount,et_ROI1,et_ROI2,et_Tenure,et_EMI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking6);
        context=this;
        init();
        initListner();
    }

    private void initListner() {
        saveandcontinue6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation()) {
                    Intent intent = new Intent(context, ActivityCreateBooking7.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean Validation() {
        if(sp_financeorcash.getSelectedItemPosition()==0){
            Toast.makeText(context,"Select finance or cash amount",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (et_financeDiscount.getText().toString().equalsIgnoreCase("")) {
            et_financeDiscount.setError("Enter finance discount");
            et_financeDiscount.requestFocus();
            return false;
        } if (et_ROI1.getText().toString().equalsIgnoreCase("")) {
            et_ROI1.setError("Enter amount");
            et_ROI1.requestFocus();
            return false;
        } if (et_ROI2.getText().toString().equalsIgnoreCase("")) {
            et_ROI2.setError("Enter amount");
            et_ROI2.requestFocus();
            return false;
        } if (et_Tenure.getText().toString().equalsIgnoreCase("")) {
            et_Tenure.setError("Enter tenure");
            et_Tenure.requestFocus();
            return false;
        }if (et_EMI.getText().toString().equalsIgnoreCase("")) {
            et_EMI.setError("Enter EMI");
            et_EMI.requestFocus();
            return false;
        }if(sp_finance.getSelectedItemPosition()==0){
            Toast.makeText(context,"Select finance",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void init() {
        saveandcontinue6=findViewById(R.id.saveandcontinue6);
        sp_financeorcash=findViewById(R.id.sp_financeorcash);
        sp_finance=findViewById(R.id.sp_finance);
        et_financeDiscount=findViewById(R.id.et_financeDiscount);
        et_ROI1=findViewById(R.id.et_ROI1);
        et_ROI2=findViewById(R.id.et_ROI2);
        et_Tenure=findViewById(R.id.et_Tenure);
        et_EMI=findViewById(R.id.et_EMI);

    }
}
