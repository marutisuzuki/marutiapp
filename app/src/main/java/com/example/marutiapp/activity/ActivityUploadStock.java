package com.example.marutiapp.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.DisplayExcelListAdapter;
import com.example.marutiapp.form.ExcelListModel;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ActivityUploadStock extends AppCompatActivity {
    TextView tv_viewdetails,tv_excelformat1,tv_excelformat2,tv_excelformat3;
    Button btn_initiateRFC,btn_uploadimg,btn_logout,btn_uploadexcelCancel,btn_uploadexcelSubmit;
    boolean doubleBackToExitPressedOnce = false;
    public ExcelListModel[] myListData1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_stock);
        this.setTitle("Upload Stock");
        init();
        initlistener();
        getdata();
    }

    private void getdata() {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedfile = data.getData(); //The uri with the location of the file
        }
    }
    private void initlistener() {
        tv_viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent  intent=new Intent(ActivityUploadStock.this,ExcelListActivity.class);
                startActivity(intent);*/
                final Dialog dialog = new Dialog(ActivityUploadStock.this);
                dialog.setContentView(R.layout.activity_recycler_excel_list);
                myListData1 = new ExcelListModel[]{
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample1.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample2.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample3.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample4.xls"),
                };
                RecyclerView recyclerView = dialog.findViewById(R.id.rv_excel_list);
                ImageView img_cancel=dialog.findViewById(R.id.img_cancel);
                DisplayExcelListAdapter adapter = new DisplayExcelListAdapter(myListData1);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(ActivityUploadStock.this));
                recyclerView.setAdapter(adapter);
                img_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_uploadimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
            }
        });
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityUploadStock.this);
                dialog.setTitle("Logout");
                dialog.setMessage("Are you sure you want to logout?");
                dialog.setCancelable(true);
                dialog.setIcon(R.drawable.ic_power_settings_new_black_24dp);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                dialog.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int
                                    id) {

                                Toast.makeText(ActivityUploadStock.this, "Logout successfully", Toast.LENGTH_SHORT).show();
                                // Continue with some operation
                                dialog.dismiss();

                            }
                        });

// A null listener allows the button to dismiss the dialog and take no further action.

                dialog.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = dialog.create();
                alert.show();

            }
        });
    }

    private void init() {
        btn_uploadexcelCancel=findViewById(R.id.btn_uploadexcelCancel);
        btn_uploadexcelSubmit=findViewById(R.id.btn_uploadexcelSubmit);
        tv_excelformat1=findViewById(R.id.tv_excelformat1);
        tv_excelformat2=findViewById(R.id.tv_excelformat2);
        tv_excelformat3=findViewById(R.id.tv_excelformat3);
        tv_viewdetails=findViewById(R.id.tv_viewdetails);
        btn_uploadimg=findViewById(R.id.btn_uploadimg);
        btn_logout=findViewById(R.id.btn_logout);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler() {
            public void postDelayed(Runnable runnable, int i) {
            }

            @Override
            public void publish(LogRecord record) {
                
            }

            @Override
            public void flush() {

            }

            @Override
            public void close() throws SecurityException {

            }
        }.postDelayed(new Runnable() {


            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}

