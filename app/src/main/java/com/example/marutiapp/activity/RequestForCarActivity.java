
package com.example.marutiapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class RequestForCarActivity extends AppCompatActivity {
    Button btn_initiateRFC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_for_car);
        this.setTitle("RFC- Request For Car");
        btn_initiateRFC=findViewById(R.id.btn_initiateRFC);
        btn_initiateRFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(RequestForCarActivity.this);
                dialog.setTitle("SaleZTrac");
                dialog.setMessage("Request for car raised successfully for booking number :SOB18001338");
                dialog.setCancelable(true);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                dialog.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int
                                    id) {

                                Toast.makeText(RequestForCarActivity.this, "Request Approved Successfully..", Toast.LENGTH_SHORT).show();
                                // Continue with some operation
                                dialog.dismiss();

                            }
                        });

// A null listener allows the button to dismiss the dialog and take no further action.

               /* dialog.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });
*/
                AlertDialog alert = dialog.create();
                alert.show();
            }
        });
    }
}
