package com.example.marutiapp.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class OnDutyForm extends AppCompatActivity {
    TextView tv_CustDetails,tv_assigndetails,tv_Ondutyformcall,tv_OndutyFormkeyNo,tv_OndutyFormlocation;
    RadioButton tv_VehicleavailableYes,tv_VehicleavailableNo,tv_VehicledamegedYes,tv_VehicledamegedNo,tv_stepneyavailableYes,tv_stepneyavailableNo,
            tv_jackavailableYes,tv_jackavailableNo,tv_firstAidKitavailableYes,tv_firstAidKitavailableNo,tv_chipfornavigationYes,tv_chipfornavigationNo,
            tv_stockyardYes,tv_stockyardNo,tv_BookkitYes,tv_BookkitNo;
    EditText et_ondutyformdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdi_approval_detail);
        this.setTitle("PDI Preliminary Checklist");
        init();
    }

    private void init() {
        tv_CustDetails=findViewById(R.id.tv_CustDetails);
        tv_assigndetails=findViewById(R.id.tv_assigndetails);
        tv_Ondutyformcall=findViewById(R.id.tv_Ondutyformcall);
        tv_OndutyFormkeyNo=findViewById(R.id.tv_OndutyFormkeyNo);
        tv_OndutyFormlocation=findViewById(R.id.tv_OndutyFormlocation);
        tv_VehicleavailableYes=findViewById(R.id.tv_VehicleavailableYes);
        tv_VehicleavailableNo=findViewById(R.id.tv_VehicleavailableNo);
        tv_VehicledamegedYes=findViewById(R.id.tv_VehicledamegedYes);
        tv_VehicledamegedNo=findViewById(R.id.tv_VehicledamegedNo);
        tv_stepneyavailableYes=findViewById(R.id.tv_stepneyavailableYes);
        tv_stepneyavailableNo=findViewById(R.id.tv_stepneyavailableNo);
        tv_jackavailableYes=findViewById(R.id.tv_jackavailableYes);
        tv_jackavailableNo=findViewById(R.id.tv_jackavailableNo);
        tv_firstAidKitavailableYes=findViewById(R.id.tv_firstAidKitavailableYes);
        tv_firstAidKitavailableNo=findViewById(R.id.tv_firstAidKitavailableNo);
        tv_chipfornavigationYes=findViewById(R.id.tv_chipfornavigationYes);
        tv_chipfornavigationNo=findViewById(R.id.tv_chipfornavigationNo);
        tv_stockyardYes=findViewById(R.id.tv_stockyardYes);
        tv_stockyardNo=findViewById(R.id.tv_stockyardNo);
        tv_BookkitYes=findViewById(R.id.tv_BookkitYes);
        tv_BookkitNo=findViewById(R.id.tv_BookkitNo);



    }
}
