package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreateBooking1 extends AppCompatActivity {
    Button saveandcontinue1;
    EditText et_cust_name, et_middelnameprebooking, et_lastnameprebooking, et_mobileNoprebooking, et_emailIDprebooking;
    ImageView et_Custphoto;
    Spinner sp_genderprebooking, sp_maritalstatusprebooking, sp_CustTypeprebooking;
    Context context;
    String[] arraygnder, arraymaritalstatus, arrayCustType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking);
        context = this;
        init();
        initListener();
    }

    private void initListener() {
        saveandcontinue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validation()) {
                    Intent intent = new Intent(context, ActivityCreatePreBooking2.class);
                    startActivity(intent);
                }
            }


        });
    }

    private void init() {
        saveandcontinue1 = findViewById(R.id.saveandcontinue1);
        et_cust_name = findViewById(R.id.et_cust_name);
        et_middelnameprebooking = findViewById(R.id.et_middelnameprebooking);
        et_lastnameprebooking = findViewById(R.id.et_lastnameprebooking);
        et_mobileNoprebooking = findViewById(R.id.et_mobileNoprebooking);
        et_emailIDprebooking = findViewById(R.id.et_emailIDprebooking);
        et_Custphoto = findViewById(R.id.et_Custphoto);
        sp_genderprebooking = findViewById(R.id.et_genderprebooking);
        sp_maritalstatusprebooking = findViewById(R.id.et_maritalstatusprebooking);
        sp_CustTypeprebooking = findViewById(R.id.et_CustTypeprebooking);

        arraygnder = new String[]{
                "Select gender", "Male", "Female",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraygnder);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_genderprebooking.setAdapter(adapter);

        arraymaritalstatus = new String[]{
                "Select marital status", "Single", "Married",
        };
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraymaritalstatus);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_maritalstatusprebooking.setAdapter(adapter1);

        arrayCustType = new String[]{
                "Select Customer type", "Abc", "xyz",
        };
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayCustType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_CustTypeprebooking.setAdapter(adapter2);
    }

    private boolean Validation() {
        if (et_cust_name.getText().toString().equalsIgnoreCase("")) {
            // Toast.makeText(context,"Enter first name",Toast.LENGTH_SHORT).show();
            et_cust_name.setError("Enter first name");
            et_cust_name.requestFocus();
            return false;
        }
        if (et_middelnameprebooking.getText().toString().equalsIgnoreCase("")) {
            // Toast.makeText(context,"Enter middle name",Toast.LENGTH_SHORT).show();
            et_middelnameprebooking.setError("Enter middle name");
            et_middelnameprebooking.requestFocus();
            return false;
        }
        if (et_lastnameprebooking.getText().toString().equalsIgnoreCase("")) {
            //Toast.makeText(context,"Enter last name",Toast.LENGTH_SHORT).show();
            et_lastnameprebooking.setError("Enter last name");
            et_lastnameprebooking.requestFocus();
            return false;
        }
        if (et_mobileNoprebooking.getText().toString().equalsIgnoreCase("")) {
            //  Toast.makeText(context,"Enter mobile no.",Toast.LENGTH_SHORT).show();
            et_mobileNoprebooking.setError("Enter mobile no.");
            et_mobileNoprebooking.requestFocus();
            return false;
        }
        if (et_emailIDprebooking.getText().toString().equalsIgnoreCase("")) {
            // Toast.makeText(context,"Enter EmailID",Toast.LENGTH_SHORT).show();
            et_emailIDprebooking.setError("Enter EmailID");
            et_emailIDprebooking.requestFocus();
            return false;
        }
        if (sp_genderprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select gender", Toast.LENGTH_SHORT).show();
        }
        if (sp_maritalstatusprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select marital status", Toast.LENGTH_SHORT).show();
        }
        if (sp_CustTypeprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select Customer type", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
