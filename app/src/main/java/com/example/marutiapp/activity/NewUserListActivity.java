package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.marutiapp.R;
import com.example.marutiapp.adapter.NewUserAdapter;
import com.example.marutiapp.form.NewUserModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class NewUserListActivity extends AppCompatActivity {
    Context context;
    public NewUserModel[] myListData1;
    RecyclerView recyclerView;
    private FloatingActionButton btnAddUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_new_user_list);
        context=this;
        btnAddUser=findViewById(R.id.floating_btn);
        this.setTitle("New User List");

        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NewUserListActivity.this,NewUserActivity.class);
                startActivity(intent);
            }
        });
         recyclerView =  findViewById(R.id.rv_newUserList);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new NewUserModel[]{


                new NewUserModel("Vivek Patil", "9989898899", "example@gmail.com","finance Manager","23-05-2020","CHOWGULE INDUSTIRES pvt.ltd","19083","NIGADI PUNE-411018","PUNE",""),
                new NewUserModel("Vivek Patil", "9989898899", "example@gmail.com","finance Manager","23-05-2020","CHOWGULE INDUSTIRES pvt.ltd","19083","NIGADI PUNE-411018","PUNE",""),
                new NewUserModel("Vivek Patil", "9989898899", "example@gmail.com","finance Manager","23-05-2020","CHOWGULE INDUSTIRES pvt.ltd","19083","NIGADI PUNE-411018","PUNE",""),
                new NewUserModel("Vivek Patil", "9989898899", "example@gmail.com","finance Manager","23-05-2020","CHOWGULE INDUSTIRES pvt.ltd","19083","NIGADI PUNE-411018","PUNE",""),
        };

        NewUserAdapter adapter = new NewUserAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }
}