package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVRTOCheckListAdapter;
import com.example.marutiapp.adapter.RVRTOIListAdapter;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public class ActivityRTOCheckApproval extends AppCompatActivity {

    private RecyclerView rvRTOCheckApproval;

    private ArrayList<FmListFom> rtoCheckListFomArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_approval);

        init();

        //  fmListFomArrayList.clear();
        rtoCheckListFomArrayList.add(new FmListFom("VIVEK U","9921088968|SOB18001944","ALTO 800 | MARUTI ALTO 800 LXI | SUPERIOR WHITE","Pending"));
        //  fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        //  mgamListFomArrayList.add(new FmListFom("M/S RAVIKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        //mgamListFomArrayList.add(new FmListFom("M/S YASHRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
        //  mgamListFomArrayList.add(new FmListFom("M/S ROOPESH TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        //     fmListFomArrayList.add(new FmListFom("M/S RAJKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (rtoCheckListFomArrayList != null && rtoCheckListFomArrayList.size() > 0) {
            rvRTOCheckApproval.setVisibility(View.GONE);
            rvRTOCheckApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvRTOCheckApproval.setLayoutManager(linearLayoutManager);
            RVRTOCheckListAdapter rtoChecklist = new RVRTOCheckListAdapter(ActivityRTOCheckApproval.this, rtoCheckListFomArrayList);

            rvRTOCheckApproval.setHasFixedSize(true);
            rvRTOCheckApproval.setNestedScrollingEnabled(false);

            rvRTOCheckApproval.setAdapter(rtoChecklist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvRTOCheckApproval =findViewById(R.id.rv_fm_list);
        rtoCheckListFomArrayList =new ArrayList<>();

    }
}


