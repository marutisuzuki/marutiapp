package com.example.marutiapp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityTVMApprovalDetails extends AppCompatActivity {
    private RadioGroup rgToggle;

    private TextView tvSubmit,tvSave;
    EditText edtComment,edtSelectDate;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvm_approval_detail);


        init();

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid())
                {
                    Toast.makeText(ActivityTVMApprovalDetails.this, "Approved Successfully", Toast.LENGTH_SHORT).show();


                }

            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid())
                {
                    Toast.makeText(ActivityTVMApprovalDetails.this, "Rejected", Toast.LENGTH_SHORT).show();

                }
            }
        });



    }

    private boolean isValid() {
        if (edtComment.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Enter Comment",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
       /* else  if (edtSelectDate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Select  Date",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }*/
        return true;

    }

    private void init() {
        edtComment=findViewById(R.id.edt_comment);
       // edtSelectDate=findViewById(R.id.edt_date);
        tvSubmit=findViewById(R.id.tv_submit);
        tvSave=findViewById(R.id.tv_save);
        //tvSubmit = findViewById(R.id.tv_submit);
    }


}

