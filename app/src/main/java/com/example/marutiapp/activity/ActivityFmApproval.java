package com.example.marutiapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.marutiapp.form.FmListFom;
import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVFmListAdapter;

import java.util.ArrayList;

public class ActivityFmApproval extends AppCompatActivity {
    private RecyclerView rvFmApproval;

    private ArrayList<FmListFom> fmListFomArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_approval);

        init();

      //  fmListFomArrayList.clear();
        fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
     //   fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
     //   fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (fmListFomArrayList != null && fmListFomArrayList.size() > 0) {
            rvFmApproval.setVisibility(View.GONE);
            rvFmApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
          //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
             LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
               rvFmApproval.setLayoutManager(linearLayoutManager);
            RVFmListAdapter fmlist = new RVFmListAdapter(ActivityFmApproval.this, fmListFomArrayList);

            rvFmApproval.setHasFixedSize(true);
            rvFmApproval.setNestedScrollingEnabled(false);

            rvFmApproval.setAdapter(fmlist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvFmApproval=findViewById(R.id.rv_fm_list);
        fmListFomArrayList=new ArrayList<>();

    }
}
