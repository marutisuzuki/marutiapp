package com.example.marutiapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import androidx.appcompat.app.AlertDialog;

public class AlertUtils {
    //  private Sessionmanager sessionmanager;

    /*Logout Alert*/
    public static void AlertDialoge(final Context context, Drawable icon, String title, String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setIcon(icon);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(context,LoginActivity.class);
                context.startActivity(intent);


                //Sessionmanager   sessionmanager = new Sessionmanager(context);
               // sessionmanager.CheckLogout();

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
