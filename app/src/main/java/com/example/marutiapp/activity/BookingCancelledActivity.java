package com.example.marutiapp.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;


public class BookingCancelledActivity extends AppCompatActivity {
    Button btn_postpond,btn_BCR,btn_CancelledRFC,btn_booking,btn_rfc,btn_stockallotment;
    TextView tv_shopName,tv_carDesc,tv_tentativeDeliverydate,tv_initiationdate,tv_raisedBy,tv_currentStatus,tv_DealerFinance,tv_exchange,tv_insurance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_followup_cancel);
        this.setTitle("Booking Cancelled");
        init();
        initListener();
    }

    private void initListener() {
        btn_postpond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(BookingCancelledActivity.this);
                dialog.setContentView(R.layout.dialog_postpond_rfc);
                ImageView dialogcancelled  =  dialog.findViewById(R.id.cancelledrfc);
                Button btnSave   = (Button) dialog.findViewById(R.id.btn_saverfc);
                Button btnCancel   = (Button) dialog.findViewById(R.id.btn_cacelrfc);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogcancelled.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    private void init() {
        btn_postpond=findViewById(R.id.btn_postpond);
        btn_BCR=findViewById(R.id.btn_BCR);
        btn_booking=findViewById(R.id.btn_booking);
        btn_CancelledRFC=findViewById(R.id.btn_CancelledRFC);
        btn_rfc=findViewById(R.id.btn_rfc);
        btn_stockallotment=findViewById(R.id.btn_stockallotment);
        tv_shopName=findViewById(R.id.tv_shopName);
        tv_carDesc=findViewById(R.id.tv_carDesc);
        tv_currentStatus=findViewById(R.id.tv_currentStatus);
        tv_initiationdate=findViewById(R.id.tv_initiationdate);
        tv_raisedBy=findViewById(R.id.tv_raisedBy);
        tv_insurance=findViewById(R.id.tv_insurance);
        tv_shopName=findViewById(R.id.tv_shopName);
        tv_tentativeDeliverydate=findViewById(R.id.tv_tentativeDeliverydate);
        tv_DealerFinance=findViewById(R.id.tv_DealerFinance);
        tv_exchange=findViewById(R.id.tv_exchange);
    }

}
