package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreateBooking5 extends AppCompatActivity {
    Button saveandcontinue5;
    Context context;
     EditText et_consumer_offer,et_RMKDiscount,et_AdditionalDiscount,et_ExchnageDiscount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking5);
        context=this;
        init();
        initListner();
    }

    private void initListner() {
        saveandcontinue5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation()) {
                    Intent intent = new Intent(context, ActivityCreateBooking6.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean Validation() {
        if (et_consumer_offer.getText().toString().equalsIgnoreCase("")) {
            et_consumer_offer.setError("Enter customer offer");
            et_consumer_offer.requestFocus();
            return false;
        }if (et_RMKDiscount.getText().toString().equalsIgnoreCase("")) {
            et_RMKDiscount.setError("Enter RMK discount");
            et_RMKDiscount.requestFocus();
            return false;
        }if (et_AdditionalDiscount.getText().toString().equalsIgnoreCase("")) {
            et_AdditionalDiscount.setError("Enter additional discount");
            et_AdditionalDiscount.requestFocus();
            return false;
        }if (et_ExchnageDiscount.getText().toString().equalsIgnoreCase("")) {
            et_ExchnageDiscount.setError("Enter exchange discount");
            et_ExchnageDiscount.requestFocus();
            return false;
        }
        return  true;
    }

    private void init() {
        saveandcontinue5=findViewById(R.id.saveandcontinue5);
        et_consumer_offer=findViewById(R.id.et_consumer_offer);
        et_RMKDiscount=findViewById(R.id.et_RMKDiscount);
        et_AdditionalDiscount=findViewById(R.id.et_AdditionalDiscount);
        et_ExchnageDiscount=findViewById(R.id.et_ExchnageDiscount);

    }
}
