package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.marutiapp.R;
import com.example.marutiapp.fragment.FragmenPreliminaryCheckList;
import com.example.marutiapp.fragment.FragmentBookingCancelation;
import com.example.marutiapp.fragment.FragmentCashier;
import com.example.marutiapp.fragment.FragmentCheckList;
import com.example.marutiapp.fragment.FragmentCoatingDept;
import com.example.marutiapp.fragment.FragmentGatePass;
import com.example.marutiapp.fragment.FragmentLeaveApplication;
import com.example.marutiapp.fragment.FragmentPendingAllotment;
import com.example.marutiapp.fragment.FragmentPendingRFCs;
import com.example.marutiapp.fragment.FragmentPriceDetails;
import com.example.marutiapp.fragment.FragmentProfileSettings;
import com.example.marutiapp.fragment.FragmentUploadStock;
import com.example.marutiapp.fragment.FragmentUserManagement;
import com.example.marutiapp.fragment.fragmentDashboard;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private boolean isStartup = true;
    Fragment fragment = null;
    private String title = "";
    private Toolbar toolbar;
    private String HR = "1", EdP="",ShowRoomManager = "2", TL = "3", DSE = "4", QCM = "5", SuperAdmin = "6", FM = "7", TVM = "8", AM = "9", AccessoryManeger = "10";
    private String DeptID;

    SharedPreferences sharedPreferences, sharedpreferencesLogin;
    SharedPreferences.Editor editorLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

       /* nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
               *//* if (isStartup) {
                    ((FrameLayout) findViewById(R.id.frame)).removeAllViews();
                    isStartup = false;
                }
*//*
                Bundle args = new Bundle();
                if (id == R.id.nav_dashboard) {
                    // itemShow.setVisible(false);
                    fragment = new fragmentDashboard();
                    title = "DashBoard";

                } else if (id == R.id.nav_upload_stock) {
                    Bundle bundle = new Bundle();
                    // itemShow.setVisible(false);
                   *//* bundle.putString("role", emp_role);
                    fragment = new FragmentAllOrders();
                    fragment.setArguments(args);
                    fragment.setArguments(bundle);
                    title = "All Orders";*//*

                } else if (id == R.id.nav_pending_rfcs) {
                    // itemShow.setVisible(false);
                  //  fragment = new FragmentToppings();
                    // fragment.setArguments(args);
                  //  title = "Toppings";

              } else if (id == R.id.nav_pending_allotment) {
                   // itemShow.setVisible(true);
                    fragment = new FragmentPendingAllotment();
                    // fragment.setArguments(args);
                    title = "Pending Allotment";

                *//*  } else if (id == R.id.nav_daily_offers) {
                    // itemShow.setVisible(false);
                    fragment = new FragmentDailyOffers();
                    fragment.setArguments(args);
                    title = "Offers";

                } else if (id == R.id.nav_add_employee) {
                    //   itemShow.setVisible(false);
                    fragment = new FragmentViewEmployee();
                    fragment.setArguments(args);
                    title = "Our Employees";

                } else if (id == R.id.nav_table) {
                    // itemShow.setVisible(false);
                    fragment = new FragmentTableDetails();
                    title = "Table Details";

                } else if (id == R.id.nav_assign_table) {
                    // itemShow.setVisible(false);
                    fragment = new FragmentAssignDetails();
                    title = "Assign Table";

                } else if (id == R.id.nav_add_payment_methods) {
                    //  itemShow.setVisible(false);
                    fragment = new FragmentPaymentMethods();
                    fragment.setArguments(args);
                    title = "Payment Methods";

                } else if (id == R.id.nav_reports) {
                    //  itemShow.setVisible(false);
                    fragment = new FragmentReports();
                    fragment.setArguments(args);
                    title = "Reports";

                } else if (id == R.id.nav_settings) {
                    // itemShow.setVisible(false);
                    fragment = new FragmentAdminSettings();
                    fragment.setArguments(args);
                    title = "Settings";
                }*//*

                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.frame, fragment);
                    ft.detach(fragment);
                    ft.attach(fragment);
                    ft.commit();
                  //  getSupportActionBar().setTitle(title);
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }

               *//* switch(id)
                {
                    case R.id.nav_home:
                        Toast.makeText(MainActivity.this, "My Account",Toast.LENGTH_SHORT).show();break;
                    case R.id.nav_gallery:
                        Toast.makeText(MainActivity.this, "Settings",Toast.LENGTH_SHORT).show();break;
                    case R.id.nav_slideshow:
                        Toast.makeText(MainActivity.this, "My Cart",Toast.LENGTH_SHORT).show();break;
                    default:
                        return true;
                }*//*


                return true;

            }
        });*/

    }
    private void init() {
        dl = (DrawerLayout) findViewById(R.id.drawer_layout);
        t = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        dl.addDrawerListener(t);
        t.syncState();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nv = (NavigationView) findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(MainActivity.this);
        Menu nav_Menu = nv.getMenu();
        Intent intent = getIntent();
        DeptID = intent.getStringExtra("CommonID");
        sharedpreferencesLogin = getApplicationContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        editorLogin = sharedpreferencesLogin.edit();
        editorLogin.putString("DeptID", DeptID);
        editorLogin.commit();
        if (DeptID.equals("1")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
          nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
          nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);
        } else if (DeptID.equals("2")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(false);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(true);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(true);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
           nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        } else if (DeptID.equals("3")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(true);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
           nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        } else if (DeptID.equals("6")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(false);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(true);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(true);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(true);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(true);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(true);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
           nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
           nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);
        } else if (DeptID.equals("7")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("11")) {
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(false);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(false);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(true);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("8")) {//TVM
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }
        else if (DeptID.equals("9")) {//ACM
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
           nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
           nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("12")) {//ALM
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
           nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(true);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(true);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(false);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("13")) {//PDI
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("14")) {//DC
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }else if (DeptID.equals("15")) {//RTO
            nav_Menu.findItem(R.id.nav_leave_app).setVisible(true);
            nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);
            nav_Menu.findItem(R.id.nav_upload_stock).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_rfcs).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_allotment).setVisible(false);
            nav_Menu.findItem(R.id.nav_preliminary_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_checklist).setVisible(true);
            nav_Menu.findItem(R.id.nav_gate_pass).setVisible(false);
            nav_Menu.findItem(R.id.nav_price_details).setVisible(false);
            nav_Menu.findItem(R.id.nav_cashier).setVisible(false);
            nav_Menu.findItem(R.id.nav_coating).setVisible(false);
            nav_Menu.findItem(R.id.nav_booking).setVisible(false);
            nav_Menu.findItem(R.id.nav_user_management).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile_setting).setVisible(true);
            nav_Menu.findItem(R.id.nav_change_password).setVisible(true);
            nav_Menu.findItem(R.id.nav_BookingInitiationList).setVisible(false);
            nav_Menu.findItem(R.id.nav_BookingCancelled).setVisible(false);
            nav_Menu.findItem(R.id.nav_pending_delivery).setVisible(false);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //  if (t.onOptionsItemSelected(item))
        //    return true;
        int id = item.getItemId();
        if (t.onOptionsItemSelected(item)) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void logout() {
        AlertUtils.AlertDialoge(this, MainActivity.this.getResources().getDrawable(R.drawable.ic_logout_red), "Logout", "Are you sure you want to logout?");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_drawer, menu);
        // itemShow = menu.findItem(R.id.action_water_bottel);

     /*   if (emp_role.equals("1")) {
            itemShow.setVisible(false);
        } else*/
        // itemShow.setVisible(true);

        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (fragment instanceof fragmentDashboard) {
                super.onBackPressed();
            } else {
                fragment = new fragmentDashboard();
                title = "DashBoard";
                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.frame, fragment);
                    ft.commit();
                    getSupportActionBar().setTitle(title);
                }
            }
        }
    }
//204769

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // return false;
        int id = item.getItemId();
               /* if (isStartup) {
                    ((FrameLayout) findViewById(R.id.frame)).removeAllViews();
                    isStartup = false;
                }
*/
        Bundle args = new Bundle();
        if (id == R.id.nav_dashboard) {
            // itemShow.setVisible(false);
            Intent intent=new Intent(MainActivity.this,DashboardActivity.class);
            startActivity(intent);
        /*   fragment = new fragmentDashboard();
            title = "DashBoard"; */

        }
        else if (id == R.id.nav_create_pre_booking) {
            // Bundle bundle = new Bundle();
            // itemShow.setVisible(false);
            //  bundle.putString("role", emp_role);
          Intent intent=new Intent(MainActivity.this,ActivityCreateBooking1.class);
          startActivity(intent);
            title = "Create Pre Booking";

        }

        else if (id == R.id.nav_upload_stock) {
            // Bundle bundle = new Bundle();
            // itemShow.setVisible(false);
            //  bundle.putString("role", emp_role);
            fragment = new FragmentUploadStock();
            fragment.setArguments(args);
            //  fragment.setArguments(bundle);
            title = "Upload Stock";

        } else if (id == R.id.nav_pending_rfcs) {
            // itemShow.setVisible(false);
     /*  fragment = new FragmentPendingRFCs();
            fragment.setArguments(args);*/

        if(DeptID.equalsIgnoreCase("7")){
                Intent intent=new Intent(MainActivity.this,Activity_FM.class);
                startActivity(intent);
                title = "Pending RFCs";
            }if(DeptID.equalsIgnoreCase("9")){
                Intent intent=new Intent(MainActivity.this,Activity_ACM.class);
                startActivity(intent);
                title = "Pending RFCs";
            }if(DeptID.equalsIgnoreCase("8")){
                Intent intent=new Intent(MainActivity.this,Activity_TVM.class);
                startActivity(intent);
                title = "Pending RFCs";
            }

        } else if (id == R.id.nav_pending_allotment) {
            // itemShow.setVisible(true);
           /* fragment = new FragmentPendingAllotment();
            fragment.setArguments(args);*/
          //  Intent intent = new Intent(this, ActivityPendingAllotment.class);
            Intent intent = new Intent(this, Activity_ALM.class);
            startActivity(intent);
            title = "Pending Allotment";
        } else if (id == R.id.nav_preliminary_checklist) {
            // itemShow.setVisible(false);
            if(DeptID.equalsIgnoreCase("7")){
                Intent intent=new Intent(MainActivity.this,Activity_FMPre.class);
                startActivity(intent);
                title = "Preliminary Checklist";
            }
            if(DeptID.equalsIgnoreCase("9")){
                Intent intent=new Intent(MainActivity.this,Activity_ACMPre.class);
                startActivity(intent);
                title = "Preliminary Checklist";
            }if(DeptID.equalsIgnoreCase("14")){
                Intent intent=new Intent(MainActivity.this,Activity_DC.class);
                startActivity(intent);
                title = "Preliminary Checklist-DC";
            }if(DeptID.equalsIgnoreCase("13")){
                Intent intent=new Intent(MainActivity.this,Activity_PDI.class);
                startActivity(intent);
                title = "Preliminary Checklist-PDI";
            }if(DeptID.equalsIgnoreCase("15")){
                Intent intent=new Intent(MainActivity.this,Activity_RTO.class);
                startActivity(intent);
                title = "Preliminary Checklist-RTO";
            }
           /* fragment = new FragmenPreliminaryCheckList();
            fragment.setArguments(args);*/


        } else if (id == R.id.nav_checklist) {
            //   itemShow.setVisible(false);
           /* fragment = new FragmentCheckList();
            fragment.setArguments(args);
            title = "Checklist";*/
            if(DeptID.equalsIgnoreCase("7")){
                Intent intent=new Intent(MainActivity.this,Activity_FMCheck.class);
                startActivity(intent);
                title = "FM Checklist";
            }
            if(DeptID.equalsIgnoreCase("14")){
                Intent intent=new Intent(MainActivity.this,Activity_DCCheck.class);
                startActivity(intent);
                title = "DC Checklist";
            }
            if(DeptID.equalsIgnoreCase("13")){
                Intent intent=new Intent(MainActivity.this,Activity_PDICheck.class);
                startActivity(intent);
                title = "PDI Checklist";
            }if(DeptID.equalsIgnoreCase("15")){
                Intent intent=new Intent(MainActivity.this,Activity_RTOCheck.class);
                startActivity(intent);
                title = "RTO Checklist";
            }
           if(DeptID.equalsIgnoreCase("9")){
                Intent intent=new Intent(MainActivity.this,Activity_ACMCheck.class);
                startActivity(intent);
                title = "ACM Checklist";
            }
         /*if(DeptID.equalsIgnoreCase("14")){
                Intent intent=new Intent(MainActivity.this,Activity_DC.class);
                startActivity(intent);
                title = "Preliminary Checklist-DC";
            }if(DeptID.equalsIgnoreCase("13")){
                Intent intent=new Intent(MainActivity.this,Activity_PDI.class);
                startActivity(intent);
                title = "Preliminary Checklist-PDI";
            }if(DeptID.equalsIgnoreCase("15")){
                Intent intent=new Intent(MainActivity.this,Activity_RTO.class);
                startActivity(intent);
                title = "Preliminary Checklist-RTO";
            }*/
        } else if (id == R.id.nav_leave_app) {
            // itemShow.setVisible(false);
          /*  fragment = new FragmentLeaveApplication();*/
            Intent intent = new Intent(this, LeaveApplicationActivity.class);
            startActivity(intent);
            title = "Leave Application";

       } else if (id == R.id.nav_BookingInitiationList) {
            // itemShow.setVisible(false);
            Intent intent = new Intent(this, BookingCarActivity.class);
            startActivity(intent);
            title = "Booking Car List";
        } else if (id == R.id.nav_BookingCancelled) {
            // itemShow.setVisible(false);
            Intent intent = new Intent(this, BookingCancelledActivity.class);
            startActivity(intent);
            title = "Booking Cancelled";
        } else if (id == R.id.nav_gate_pass) {
            // itemShow.setVisible(false);
                 /*   fragment = new FragmentGatePass();
                    title = "Gate Pass";*/
            Intent intent = new Intent(this, ActivityGatePass.class);
            startActivity(intent);
            title = "Gate Pass";
        } else if (id == R.id.nav_price_details) {
            //  itemShow.setVisible(false);
                 /*   fragment = new FragmentPriceDetails();
                    fragment.setArguments(args);*/
            Intent intent = new Intent(this, ActivityPriceDetails.class);
            startActivity(intent);
            title = "Price Details";

        } else if (id == R.id.nav_user_management) {
            //  itemShow.setVisible(false);
           /* fragment = new FragmentUserManagement();
            fragment.setArguments(args);*/
            Intent intent = new Intent(this, NewUserActivity.class);
            startActivity(intent);
            title = "User Management";

        } else if (id == R.id.nav_cashier) {
            //  itemShow.setVisible(false);
         /*   fragment = new FragmentCashier();
            fragment.setArguments(args);*/
            Intent intent = new Intent(this, CashierPage.class);
            startActivity(intent);
            title = "Cashier";

        } else if (id == R.id.nav_change_password) {
            Intent intent = new Intent(this, ChangeActivity.class);
            startActivity(intent);
            title = "Cashier";
        } else if (id == R.id.nav_coating) {
            //  itemShow.setVisible(false);
            Intent intent = new Intent(this, CoatingDeptActivity.class);
            startActivity(intent);
            title = "Coating Department";

        } else if (id == R.id.nav_booking) {
            //  itemShow.setVisible(false);
          //  fragment = new FragmentBookingCancelation();
          //  fragment.setArguments(args);
            Intent intent = new Intent(this, BookingCancellationActivity.class);
            startActivity(intent);
           // title = "Coating Department";
            title = "Booking Cancellation";

        } else if (id == R.id.nav_profile_setting) {
            // itemShow.setVisible(false);
                   /* fragment = new FragmentProfileSettings();
                    fragment.setArguments(args);*/
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            title = "Profile";
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.frame, fragment);
            ft.detach(fragment);
            ft.attach(fragment);
            ft.commit();
            getSupportActionBar().setTitle(title);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

     /*extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        *//*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*//*

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

           *//* if (fragment instanceof fragmentDashboard) {
                super.onBackPressed();
            } else {*//*
            Intent  intent=new Intent(MainActivity.this,DashboardActivity.class);
            startActivity(intent);
                *//*if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.screen_area, fragment);
                    ft.commit();
                    getSupportActionBar().setTitle(title);
                }*//*
           // }
        }
    }

  *//*  @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*//*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

       *//* } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
*//*
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("","Dashboard", true, false, "https://www.journaldev.com/9333/android-webview-example-tutorial"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("","Upload Stock", true, true, ""); //Menu of Java Tutorials
        headerList.add(menuModel);

        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("","Stock Upload", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","Available Stock", false, false, "");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

         *//*menuModel = new MenuModel("","Salesztrac", true, false, ""); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*//*

     //   menuModel = new MenuModel("","Salesztrac", true, true, ""); //Menu of Java Tutorials
     //   headerList.add(menuModel);

       *//* childModelsList = new ArrayList<>();
        menuModel = new MenuModel("","Salesztrac", true, true, ""); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("","Booking Car", false, false, "https://www.journaldev.com/19243/python-ast-abstract-syntax-tree");
        childModelsList.add(childModel);

        childModel = new MenuModel("","Request For Car", false, false, "https://www.journaldev.com/19226/python-fractions");
        childModelsList.add(childModel);

        childModel = new MenuModel("","Cancelled Booking", false, false, "https://www.journaldev.com/19226/python-fractions");
        childModelsList.add(childModel);




        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }*//*


     *//*  if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }


        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
*//*

        menuModel = new MenuModel("","Pending RFCs", true, true, ""); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModelsList = new ArrayList<>();
        childModel = new MenuModel("","ACM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","FM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","TVM", false, false, "");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }
       // childModelsList = new ArrayList<>();
        menuModel = new MenuModel("","Pending Allotment", true, false, ""); //Menu of Python Tutorials
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }


        menuModel = new MenuModel("","Preliminary Checklist", true, true, ""); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModelsList = new ArrayList<>();
        childModel = new MenuModel("","ACM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","FM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","MGAM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","DC", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","RTO", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","PDI", false, false, "");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel("","Checklist", true, true, ""); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModelsList = new ArrayList<>();
        childModel = new MenuModel("","ACM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","FM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","MGAM", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","DC", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","RTO", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","PDI", false, false, "");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel("","Leave Application", true, false, ""); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("","Gate Pass", true, false, ""); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("","Price Details", true, false, ""); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
        menuModel = new MenuModel("","Uesr Management", true, false, ""); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }



        menuModel = new MenuModel("","Profile Setting", true, true, ""); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModelsList = new ArrayList<>();
        childModel = new MenuModel("","Profile", false, false, "");
        childModelsList.add(childModel);

        childModel = new MenuModel("","Change Password", false, false, "");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("","Logout", true, false, ""); //Menu of Python Tutorials
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }



    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

       *//* expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        WebView webView = findViewById(R.id.webView);
                        webView.loadUrl(headerList.get(groupPosition).url);
                        onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    if (model.url.length() > 0) {
                        WebView webView = findViewById(R.id.webView);
                        webView.loadUrl(model.url);
                        onBackPressed();
                    }
                }

                return false;
            }
        });*//*
    }
*/
}