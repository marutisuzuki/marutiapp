package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.marutiapp.R;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity { private ImageView btnSend;
    private EditText edtMob, edtOtp;
    private TextView tvResdOtp, tvOtpTime;
    private String HR = "1", ShowRoomManager = "2", TL = "3", DSE = "4", QCM = "5", SuperAdmin = "6", FM = "7", TVM = "8", ACM = "9", AccessoryManeger = "10",CM="11",ALM="12",
    PDI="13",DC="14",RTO="15";

    String Message = "True";
    Context context;
    TextInputEditText til_Username, til_OTP;
    String UserID, Dept_ID;
    SharedPreferences sharedPreferences, sharedpreferencesLogin;
    SharedPreferences.Editor editorLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        init();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(edtOtp.getText().toString().length()==0) {
                    edtOtp.setVisibility(View.VISIBLE);
                    tvOtpTime.setVisibility(View.VISIBLE);
                    tvResdOtp.setVisibility(View.VISIBLE);

                    btnSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {*/
/*
                            if (edtOtp.getText().toString().length() == 0) {
                                Toast.makeText(LoginActivity.this, "Plase Enter Otp", Toast.LENGTH_SHORT).show();
                            } else {*/
                if (Validation()) {
                    if (til_Username.getText().toString().equalsIgnoreCase("HR") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "1");
                        startActivity(intent);

                    } else if (til_Username.getText().toString().equalsIgnoreCase("SM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "2");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("TL") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "3");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("DSE") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "4");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("QCM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "5");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("SuperAdmin") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "6");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("FM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "7");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("TVM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "8");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("ACM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "9");
                        startActivity(intent);
                    } else if (til_Username.getText().toString().equalsIgnoreCase("AccessoryManeger") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "10");
                        startActivity(intent);
                    }else if (til_Username.getText().toString().equalsIgnoreCase("CM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "11");
                        startActivity(intent);
                    }else if (til_Username.getText().toString().equalsIgnoreCase("ALM") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "12");
                        startActivity(intent);
                    }else if (til_Username.getText().toString().equalsIgnoreCase("PDI") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "13");
                        startActivity(intent);
                    }else if (til_Username.getText().toString().equalsIgnoreCase("DC") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "14");
                        startActivity(intent);
                    }else if (til_Username.getText().toString().equalsIgnoreCase("RTO") && til_OTP.getText().toString().equalsIgnoreCase("12")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("CommonID", "15");
                        startActivity(intent);
                    }


                }            /*Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);*/
            }
        });

    }

    private boolean Validation() {
        String regex = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{333333339}$";
        if (til_Username.length() == 0 /*|| til_Username.length() < 10*/) {
            Toast.makeText(getApplicationContext(), "Enter Correct UserName", Toast.LENGTH_LONG).show();
            return false;
        }
        if (til_OTP.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter OTP", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    private void init() {
        til_OTP = findViewById(R.id.til_OTP);
        til_Username = findViewById(R.id.til_Username);
        btnSend = findViewById(R.id.btn_send);
    }
}
