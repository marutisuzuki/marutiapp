package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;


public class BookingCancellationActivity extends AppCompatActivity {
    Context context;
    Button btn_bookingCancelled;
    TextView tv_CustomerID, tv_CustomerName, tv_BookingDate, tv_Advancepayment;
    Spinner sp_selectCustomer, sp_BookingVehicle;
    EditText et_CancellationCharges, tv_CancellationReason, tv_AccountNo, tv_IFSCCode;
    String[] arraySpinner,BookingVehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_cancellation);
        context = this;
        this.setTitle("Booking Cancellation");
        init();
        initListener();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initListener() {
        btn_bookingCancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    Toast.makeText(BookingCancellationActivity.this, "Booking Cancelled Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean validation() {
        if (sp_selectCustomer.getSelectedItem().toString().trim().equalsIgnoreCase("Select Customer")) {
            Toast.makeText(BookingCancellationActivity.this, "Select Customer", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (et_CancellationCharges.getText().toString().equalsIgnoreCase("")) {
            et_CancellationCharges.setError("Enter Cancellation Charges");
            return false;
        }
        if (sp_BookingVehicle.getSelectedItem().toString().trim().equalsIgnoreCase("Select Vehicle")) {
            Toast.makeText(BookingCancellationActivity.this, "Select Vehicle", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tv_CancellationReason.getText().toString().equalsIgnoreCase("")) {
            tv_CancellationReason.setError("Enter Cancellation Reason");
            return false;
        }
        if (tv_AccountNo.getText().toString().equalsIgnoreCase("")) {
            tv_AccountNo.setError("Enter Account Number");
            return false;
        }
        if (tv_IFSCCode.getText().toString().equalsIgnoreCase("")) {
            tv_IFSCCode.setError("Enter IFSC code");
            return false;
        }
        return true;
    }

    private void init() {
        btn_bookingCancelled = findViewById(R.id.btn_bookingCancelled);
        tv_CustomerID = findViewById(R.id.tv_CustomerID);
        tv_CustomerName = findViewById(R.id.tv_CustomerName);
        tv_BookingDate = findViewById(R.id.tv_BookingDate);
        tv_Advancepayment = findViewById(R.id.tv_Advancepayment);
        sp_selectCustomer = findViewById(R.id.sp_selectCustomer);
        et_CancellationCharges = findViewById(R.id.et_CancellationCharges);
        sp_BookingVehicle = findViewById(R.id.sp_BookingVehicle);
        tv_CancellationReason = findViewById(R.id.tv_CancellationReason);
        tv_AccountNo = findViewById(R.id.tv_AccountNo);
        tv_IFSCCode = findViewById(R.id.tv_IFSCCode);
        arraySpinner = new String[]{
                "Select Customer", "Vivek Patil", "Patil S."
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_selectCustomer.setAdapter(adapter);

        BookingVehicle=new String[]{
               "Select Vehicle", "Maruti Suzuki","Swift"
        };
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, BookingVehicle);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_selectCustomer.setAdapter(adapter1);
    }
}