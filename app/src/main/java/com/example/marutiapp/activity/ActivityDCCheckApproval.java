package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVDCCheckListAdapter;
import com.example.marutiapp.adapter.RVRTOCheckListAdapter;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public  class ActivityDCCheckApproval extends AppCompatActivity {

    private RecyclerView rvDCCheckApproval;

    private ArrayList<FmListFom> dcCheckListFomArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_approval);

        init();

        //  fmListFomArrayList.clear();
        dcCheckListFomArrayList.add(new FmListFom("VIVEK U","9921088968|SOB18001944","ALTO 800 | MARUTI ALTO 800 LXI | SUPERIOR WHITE","Pending"));
        //  fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        //  mgamListFomArrayList.add(new FmListFom("M/S RAVIKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        //mgamListFomArrayList.add(new FmListFom("M/S YASHRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
        //  mgamListFomArrayList.add(new FmListFom("M/S ROOPESH TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        //     fmListFomArrayList.add(new FmListFom("M/S RAJKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (dcCheckListFomArrayList != null && dcCheckListFomArrayList.size() > 0) {
            rvDCCheckApproval.setVisibility(View.GONE);
            rvDCCheckApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvDCCheckApproval.setLayoutManager(linearLayoutManager);
            RVDCCheckListAdapter dcChecklist = new RVDCCheckListAdapter(ActivityDCCheckApproval.this, dcCheckListFomArrayList);

            rvDCCheckApproval.setHasFixedSize(true);
            rvDCCheckApproval.setNestedScrollingEnabled(false);

            rvDCCheckApproval.setAdapter(dcChecklist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvDCCheckApproval =findViewById(R.id.rv_fm_list);
        dcCheckListFomArrayList =new ArrayList<>();

    }
}



