package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreatePreBooking3 extends AppCompatActivity {
    Button saveandcontinue3;
    Context context;
    EditText et_Addr1,et_Addrline2prebooking,et_Addrline3prebooking,comment,et_firstNameshipto,et_MiddleNameshipto,et_LastNameshipto,et_Addr1shipto,et_Addrline2shipto;
    Spinner sp_stateprebooking,sp_cityprebooking,sp_Districtprebooking,sp_Tehsilprebooking,sp_villegeprebooking,sp_Pincodeprebooking;
    RadioButton rb_createbookyes,rb_createbookno;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking3);
        context=this;
        init();
        initListner();
    }

    private void initListner() {
        saveandcontinue3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation()) {
                    Intent intent = new Intent(context, ActivityCreatePreBooking4.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean Validation() {
        if (et_Addr1.getText().toString().equalsIgnoreCase("")) {
            et_Addr1.setError("Enter Address");
            et_Addr1.requestFocus();
            return false;
        }else if (sp_stateprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select state", Toast.LENGTH_SHORT).show();
        } else if (sp_cityprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select city", Toast.LENGTH_SHORT).show();
        }else if (sp_Districtprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select District", Toast.LENGTH_SHORT).show();
        }else if (sp_Tehsilprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select tehsil", Toast.LENGTH_SHORT).show();
        }else if (sp_villegeprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select villege", Toast.LENGTH_SHORT).show();
        }else if (sp_Pincodeprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select pincode", Toast.LENGTH_SHORT).show();
        }
        else if (comment.getText().toString().equalsIgnoreCase("")) {
            comment.setError("Enter Comment");
            comment.requestFocus();
            return false;
        }else if (et_firstNameshipto.getText().toString().equalsIgnoreCase("")) {
            et_firstNameshipto.setError("Enter first name");
            et_firstNameshipto.requestFocus();
            return false;
        } else if (et_MiddleNameshipto.getText().toString().equalsIgnoreCase("")) {
            et_MiddleNameshipto.setError("Enter middle name");
            et_MiddleNameshipto.requestFocus();
            return false;
        }else if (et_LastNameshipto.getText().toString().equalsIgnoreCase("")) {
            et_LastNameshipto.setError("Enter last name");
            et_LastNameshipto.requestFocus();
            return false;
        }else if (et_Addr1shipto.getText().toString().equalsIgnoreCase("")) {
            et_Addr1shipto.setError("Enter address line1");
            et_Addr1shipto.requestFocus();
            return false;
        }else if (et_Addrline2shipto.getText().toString().equalsIgnoreCase("")) {
            et_Addrline2shipto.setError("Enter address line2");
            et_Addrline2shipto.requestFocus();
            return false;
        }
        return true;
    }

    private void init() {
        saveandcontinue3=findViewById(R.id.saveandcontinue3);
        rb_createbookyes=findViewById(R.id.rb_createbookyes);
        rb_createbookno=findViewById(R.id.rb_createbookno);
        sp_stateprebooking=findViewById(R.id.sp_stateprebooking);
        sp_cityprebooking=findViewById(R.id.sp_cityprebooking);
        sp_Districtprebooking=findViewById(R.id.sp_Districtprebooking);
        sp_Tehsilprebooking=findViewById(R.id.sp_Tehsilprebooking);
        sp_villegeprebooking=findViewById(R.id.sp_villegeprebooking);
        sp_Pincodeprebooking=findViewById(R.id.sp_Pincodeprebooking);
        et_Addr1=findViewById(R.id.et_Addr1);
        et_Addrline2prebooking=findViewById(R.id.et_Addrline2prebooking);
        et_Addrline3prebooking=findViewById(R.id.et_Addrline3prebooking);
        comment=findViewById(R.id.comment);
        et_firstNameshipto=findViewById(R.id.et_firstNameshipto);
        et_MiddleNameshipto=findViewById(R.id.et_MiddleNameshipto);
        et_LastNameshipto=findViewById(R.id.et_LastNameshipto);
        et_Addr1shipto=findViewById(R.id.et_Addr1shipto);
        et_Addrline2shipto=findViewById(R.id.et_Addrline2shipto);
    }
}
