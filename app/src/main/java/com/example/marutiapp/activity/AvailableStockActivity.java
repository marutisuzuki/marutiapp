package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.AvailableStockAdapter;
import com.example.marutiapp.form.AvailableStockModel;

public class AvailableStockActivity extends AppCompatActivity {
    Context context;
    public AvailableStockModel[] myListData1;
    String[] arraySpinner,arrayvarient,arraycolor;
    RecyclerView recyclerView;
     LinearLayoutManager layoutManager;
     Spinner sp_color,sp_model,sp_varient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_stock);
        context=this;
        this.setTitle("Available Stock");
        init();

        myListData1 = new AvailableStockModel[]{
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "12","1111"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "13","233"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "14","142"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "15","123"),
        };

        AvailableStockAdapter adapter = new AvailableStockAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void init() {
         recyclerView = (RecyclerView) findViewById(R.id.rv_availableStock);
        sp_color = findViewById(R.id.sp_color);
        sp_model = findViewById(R.id.sp_model);
        sp_varient = findViewById(R.id.sp_varient);
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        arraySpinner = new String[]{
                "Select Model", "abc", "abc1",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_model.setAdapter(adapter);

        arrayvarient = new String[]{
                "Select Varient", "abc", "abc1",
        };
        ArrayAdapter<String> adaptervarient = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayvarient);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_varient.setAdapter(adaptervarient);


        arraycolor = new String[]{
                "Select Color", "BLUE", "RED","GREEN",
        };
        ArrayAdapter<String> adaptercolor = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraycolor);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_color.setAdapter(adaptercolor);
    }
}