package com.example.marutiapp.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Activity_TVM extends AppCompatActivity {
    private View view;
    private RecyclerView rvFm;
    private EditText edtBookingNo;
    private LinearLayout rgToggle;
    Context context;
    private TextView tvSearch,tvReset;
    private String seletedStartDate, seletedEndDate, getStartDate;
    private Calendar mcurrentDate;
    private EditText etvFromDate, etvToDate;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_fm);
        context=this;
        init();

        etvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(Activity_TVM.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        // String myFormat = "yyyy-MM-dd h:mm:ss a";
                        String myFormat = "dd-MM-yyyy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                        getStartDate = sdf.format(myCalendar.getTime());
                        etvFromDate.setText(getStartDate);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String formattedDate = df.format(c);
                try {
                    Date d = df.parse(formattedDate);
                    mDatePicker.getDatePicker().setMinDate(d.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                mDatePicker.show();
            }
        });
        etvToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                seletedStartDate = getStartDate;


                if (seletedStartDate != null) {
                    DatePickerDialog mDatePicker1 = new DatePickerDialog(Activity_TVM.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                            Calendar myCalendar = Calendar.getInstance();
                            myCalendar.set(Calendar.YEAR, selectedyear);
                            myCalendar.set(Calendar.MONTH, selectedmonth);
                            myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                            //   String myFormat = "yyyy-MM-dd h:mm:ss a"; //Change as you need
                            String myFormat = "dd-MM-yyyy"; //Change as you need
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);

                            Date startDate = null, endDate = null;
                            try {
                                String getEndDate = sdf.format(myCalendar.getTime());
                                endDate = sdf.parse(getEndDate);
                                startDate = sdf.parse(seletedStartDate);
                                if (startDate.after(endDate)) {
                                    Toast.makeText(Activity_TVM.this, R.string.select_end_greater_start_date, Toast.LENGTH_SHORT).show();
                                } else {
                                    etvToDate.setText(sdf.format(myCalendar.getTime()));
                                    seletedEndDate = sdf.format(myCalendar.getTime());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);

                    Date date = Calendar.getInstance().getTime();
                    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    String formattedDate1 = sdformat.format(date);
                    try {
                        Date d = sdformat.parse(formattedDate1);
                        mDatePicker1.getDatePicker().setMinDate(d.getTime());

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    mDatePicker1.show();
                } else {
                    Toast.makeText(Activity_TVM.this, R.string.select_first_start_date, Toast.LENGTH_SHORT).show();
                }
            }
        });



      /*  rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:
                       *//* llApplyLeave.setVisibility(View.VISIBLE);
                        flLeaveHistory.setVisibility(View.GONE);*//*
                        break;

                    case R.id.rd_search:

                        Intent intent=new Intent(context, ActivityDCPreApproval.class);
                        startActivity(intent);
                      *//*  llApplyLeave.setVisibility(View.GONE);
                        flLeaveHistory.setVisibility(View.VISIBLE);
                        getLeaveHistory();*//*
                        break;
                }
            }
        });*/


        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {

                    Intent intent = new Intent(context, ActivityTVMApproval.class);
                    startActivity(intent);
                }
            }
        });
        tvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private boolean isValid() {

        if (edtBookingNo.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(context,"Enter Booking No",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        else  if (etvFromDate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(context,"Select From Date",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        else if (etvToDate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(context,"Select To Date",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        // if(tvFromDate.set)
        return  true;
    }


    private void init() {

        rgToggle = findViewById(R.id.rgToggle);
        edtBookingNo=findViewById(R.id.edt_booking_no);
        etvFromDate=findViewById(R.id.etv_from_date);
        etvToDate=findViewById(R.id.etv_to_date);
        tvSearch=findViewById(R.id.rd_search);
        tvReset=findViewById(R.id.rd_reset);
    }
}
