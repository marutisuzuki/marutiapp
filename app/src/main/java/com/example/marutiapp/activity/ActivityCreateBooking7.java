package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.marutiapp.R;
public class ActivityCreateBooking7 extends AppCompatActivity {
    Button saveandcontinue7;
    Context context;
    EditText et_BookingAmt,et_RefnuNo;
    Spinner sp_BpaymentMode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking7);
        context=this;
        init();
        initListner();
    }

    private void initListner() {
        saveandcontinue7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation()) {
                    Intent intent = new Intent(context, ActivityCreateBooking8.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean Validation() {
        if(et_BookingAmt.getText().toString().equalsIgnoreCase("")){
            et_BookingAmt.setError("Enter booking amount");
            et_BookingAmt.requestFocus();
            return false;
        }else if(sp_BpaymentMode.getSelectedItemPosition()==0){
            Toast.makeText(context,"Enter payment mode",Toast.LENGTH_SHORT).show();
            return false;
        }else if(et_RefnuNo.getText().toString().equalsIgnoreCase("")){
            et_RefnuNo.setError("Enter refrence no.");
            et_RefnuNo.requestFocus();
            return false;
        }

        return  true;
    }

    private void init() {
        saveandcontinue7=findViewById(R.id.saveandcontinue7);
        et_BookingAmt=findViewById(R.id.et_BookingAmt);
        et_RefnuNo=findViewById(R.id.et_RefnuNo);
        sp_BpaymentMode=findViewById(R.id.sp_BpaymentMode);
    }
}
