package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreateBooking10 extends AppCompatActivity {
    Button finalsubmit;
    Context context;
    EditText et_BookingAmt,et_tentativedeliverydate;
    TextView tv_sendotp,tv_addsignature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking8);
        context=this;
        init();
        initListner();
    }

    private void initListner() {
        finalsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void init() {
        finalsubmit=findViewById(R.id.finalsubmit);
        et_BookingAmt=findViewById(R.id.et_BookingAmt);
        et_tentativedeliverydate=findViewById(R.id.et_tentativedeliverydate);
        tv_sendotp=findViewById(R.id.tv_sendotp);
        tv_addsignature=findViewById(R.id.tv_addsignature);

    }
}
