package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.BookingcarAdapter;


public class BookingFollowupActivity extends AppCompatActivity {
    TextView tv_followupName,tv_followupDesc,tv_followupTDDate,tv_followupCustContact,tv_followupDetails,tv_followupdatetime,tv_followupremark,tv_Make,
            tv_Model,tv_Evaluator,tv_ExpectedPrice,tv_EvaluatedPrice,tv_BuyingPrice;
    Button tv_followupBCR,tv_followupRFC,tv_followupBCRDetails,tv_followupRFCDetails,tv_followupallotmentdetails;
    BookingcarAdapter bookingcarAdapter;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_followup1);
        context=this;
        init();
        initlistener();
    }

    private void initlistener() {
        bookingcarAdapter = new BookingcarAdapter(context);
        tv_followupRFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,RequestForCarActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        tv_followupName=findViewById(R.id.tv_followupName);
        tv_followupDesc=findViewById(R.id.tv_followupDesc);
        tv_followupTDDate=findViewById(R.id.tv_followupTDDate);
        tv_followupCustContact=findViewById(R.id.tv_followupCustContact);
        tv_followupDetails=findViewById(R.id.tv_followupDetails);
        tv_followupdatetime=findViewById(R.id.tv_followupdatetime);
        tv_followupremark=findViewById(R.id.tv_followupremark);
        tv_Make=findViewById(R.id.tv_Make);
        tv_Model=findViewById(R.id.tv_Model);
        tv_Evaluator=findViewById(R.id.tv_Evaluator);
        tv_ExpectedPrice=findViewById(R.id.tv_ExpectedPrice);
        tv_EvaluatedPrice=findViewById(R.id.tv_EvaluatedPrice);
        tv_BuyingPrice=findViewById(R.id.tv_BuyingPrice);
        tv_followupBCR=findViewById(R.id.tv_followupBCR);
        tv_followupRFC=findViewById(R.id.tv_followupRFC);
        tv_followupBCRDetails=findViewById(R.id.tv_followupBCRDetails);
        tv_followupRFCDetails=findViewById(R.id.tv_followupRFCDetails);
        tv_followupallotmentdetails=findViewById(R.id.tv_followupallotmentdetails);
    }

}
