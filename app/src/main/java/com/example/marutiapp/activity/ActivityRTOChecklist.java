package com.example.marutiapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityRTOChecklist  extends AppCompatActivity {
    private RadioGroup rgToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_checklist);
        init();

        rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:
                       /* llApplyLeave.setVisibility(View.VISIBLE);
                        flLeaveHistory.setVisibility(View.GONE);*/
                        break;

                    case R.id.rd_search:

                        Intent intent=new Intent(ActivityRTOChecklist.this, ActivityRTOCheckApproval.class);
                        startActivity(intent);
                      /*  llApplyLeave.setVisibility(View.GONE);
                        flLeaveHistory.setVisibility(View.VISIBLE);
                        getLeaveHistory();*/
                        break;
                }
            }
        });
    }

    private void init() {
        // rvFm=findViewById(R.id.rv_fm);
        rgToggle = findViewById(R.id.rgToggle);
    }
}





