package com.example.marutiapp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityRTOChecklistApprovalDetails  extends AppCompatActivity {
    private TextView tvSubmit,tvSave;
    EditText edtComment,edtSelectDate;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rto_checklist_approval_detail);
        init();

        tvSubmit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isValid())
            {

                AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityRTOChecklistApprovalDetails.this);
                dialog.setTitle("Action Confirmation");
                dialog.setMessage("Are you sure to want to Submit?");
                dialog.setCancelable(true);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                dialog.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int
                                    id) {
                                Toast.makeText(ActivityRTOChecklistApprovalDetails.this, "Submitted Successfully", Toast.LENGTH_SHORT).show();


                                //     Toast.makeText(ActivityMGAMApprovalDetails.this, "Approved Successfully..", Toast.LENGTH_SHORT).show();
                                // Continue with some operation
                                //    dialog.dismiss();

                            }
                        });

// A null listener allows the button to dismiss the dialog and take no further action.

                dialog.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = dialog.create();
                alert.show();



            }

        }
    });

        tvSave.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isValid())
            {
                Toast.makeText(ActivityRTOChecklistApprovalDetails.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
            }
        }
    });


}

    private boolean isValid() {
       /* if (edtComment.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Enter Comment",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }*/
        return true;

    }

    private void init() {
        edtComment=findViewById(R.id.edt_comment);
        edtSelectDate=findViewById(R.id.edt_date);
        tvSubmit=findViewById(R.id.tv_submit);
        tvSave=findViewById(R.id.tv_save);
    }
}










