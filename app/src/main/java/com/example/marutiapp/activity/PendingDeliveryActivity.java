package com.example.marutiapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class PendingDeliveryActivity  extends AppCompatActivity {
    private  RadioGroup rgToggle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_delivery);

        init();

        rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:
                       /* llApplyLeave.setVisibility(View.VISIBLE);
                        flLeaveHistory.setVisibility(View.GONE);*/
                        break;

                    case R.id.rd_search:

                        Intent intent=new Intent(PendingDeliveryActivity.this, ActivityPendingDeliveryList.class);
                        startActivity(intent);
                      /*  llApplyLeave.setVisibility(View.GONE);
                        flLeaveHistory.setVisibility(View.VISIBLE);
                        getLeaveHistory();*/
                        break;
                }
            }
        });
    }

    private void init() {
        rgToggle = findViewById(R.id.rgToggle);
    }
}
