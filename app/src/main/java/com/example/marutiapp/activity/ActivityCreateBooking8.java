package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreateBooking8 extends AppCompatActivity {
    Button saveandcontinue8;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_booking9);
        context=this;
        init();
        initListner();
    }
    private void initListner() {
        saveandcontinue8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ActivityCreateBooking10.class);
                startActivity(intent);
            }
        });
    }
    private void init() {
        saveandcontinue8=findViewById(R.id.saveandcontinue8);
    }
}
