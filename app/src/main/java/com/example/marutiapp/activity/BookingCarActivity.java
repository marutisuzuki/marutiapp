package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.BookingcarAdapter;
import com.example.marutiapp.form.BookingCarModel;

public class BookingCarActivity extends AppCompatActivity {
    Context context;
    public BookingCarModel[] myListData1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_bookingcar_list);
        context = this;
        this.setTitle("Booking Car");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_bookingcarlist);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new BookingCarModel[]{
                new BookingCarModel("04-05-2020 05:45", "", "MR DHANAJI EKNATH KONDE", "VITARA BREZZA| MARUTI VITARA BREZZA|MET,PREMIUM SILVER", "yes"),
                new BookingCarModel("05-05-2020 05:45", "", "MR DHANAJI EKNATH KONDE", "VITARA BREZZA| MARUTI VITARA BREZZA|MET,PREMIUM SILVER", "no"),
                new BookingCarModel("06-05-2020 05:45", "", "MR DHANAJI EKNATH KONDE", "VITARA BREZZA| MARUTI VITARA BREZZA|MET,PREMIUM SILVER", "yes"),
                new BookingCarModel("09-05-2020 05:45", "", "MR DHANAJI EKNATH KONDE", "VITARA BREZZA| MARUTI VITARA BREZZA|MET,PREMIUM SILVER", "yes"),
        };

        BookingcarAdapter adapter = new BookingcarAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }
}