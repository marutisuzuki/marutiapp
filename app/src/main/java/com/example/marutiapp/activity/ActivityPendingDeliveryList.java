package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVPendingDeliveryListAdapter;

import com.example.marutiapp.form.PendingDeliveryForm;

import java.util.ArrayList;

public class ActivityPendingDeliveryList  extends AppCompatActivity {
    private RecyclerView rvPendingDeliveryList;
    private ArrayList<PendingDeliveryForm> arrayListPendingDelivery;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_delivery_list);

        init();

        //  fmListFomArrayList.clear();
        arrayListPendingDelivery.add(new PendingDeliveryForm("VIVEK U","9921088210|SOB18001330","Shivaji Nagar","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Monali Sopan mazire 9763745678","30-09-2018","Pending"));


        if (arrayListPendingDelivery != null && arrayListPendingDelivery.size() > 0) {
            rvPendingDeliveryList.setVisibility(View.GONE);
            rvPendingDeliveryList.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvPendingDeliveryList.setLayoutManager(linearLayoutManager);
            RVPendingDeliveryListAdapter pendingDeliveryList = new RVPendingDeliveryListAdapter(ActivityPendingDeliveryList.this, arrayListPendingDelivery);

            rvPendingDeliveryList.setHasFixedSize(true);
            rvPendingDeliveryList.setNestedScrollingEnabled(false);

            rvPendingDeliveryList.setAdapter(pendingDeliveryList);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvPendingDeliveryList =findViewById(R.id.rv_pending_list);
        arrayListPendingDelivery =new ArrayList<>();

    }
}




