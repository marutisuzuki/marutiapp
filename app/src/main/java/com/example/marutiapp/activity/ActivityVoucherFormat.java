package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVAdapterUnitView;
import com.example.marutiapp.form.VoucherInfo;

import java.util.ArrayList;

public class ActivityVoucherFormat extends AppCompatActivity {
    private Button tvAddInfo;

    private View dialoglayout, dialogLayoutField;
   // private BottomSheetDialog bottomSheetDialog;
    private AlertDialog alertDialog;
    private ArrayList<EditText> arrayListUnitName;
    private ArrayList<EditText> arrayListUnitPrice;
    private RecyclerView rvUnit;


    private ArrayList<VoucherInfo> arrayListUnit;

    private String image_result, mFinalImageName, image;
    EditText unitName, unitPrice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_format);
        init();

        tvAddInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                dialogLayoutField = li.inflate(R.layout.unit_diaplay_fields, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(ActivityVoucherFormat.this);
                builder.setView(dialogLayoutField);
                alertDialog = builder.create();

                Button save = dialogLayoutField.findViewById(R.id.btnSave);
                Button cancel = dialogLayoutField.findViewById(R.id.btnCancel);

                unitName = dialogLayoutField.findViewById(R.id.etv_unit_name);
                unitPrice = dialogLayoutField.findViewById(R.id.etv_unit_price);

//                if(unitName.getText().toString().length()==0 && unitPrice.getText().toString().length()==0){
//                    alertMsg();
//
//                }else {


                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        VoucherInfo unitForm = new VoucherInfo();
                        unitForm.setName(unitName.getText().toString());
                        String price = unitPrice.getText().toString();
                        unitForm.setPrice(Integer.parseInt(price));
                        arrayListUnit.add(unitForm);
                        alertDialog.dismiss();

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityVoucherFormat.this, LinearLayoutManager.VERTICAL, false);
                        rvUnit.setHasFixedSize(true);
                        rvUnit.setLayoutManager(linearLayoutManager);
                        RVAdapterUnitView adapterUnitView = new RVAdapterUnitView(getApplicationContext(), arrayListUnit, new DeleteListener() {
                            @Override
                            public void getDeleteListenerPosition(int position) {
                                arrayListUnit.remove(position);
                            }
                        });
                        rvUnit.setAdapter(adapterUnitView);
                        adapterUnitView.notifyDataSetChanged();
                    }
                });
                alertDialog.dismiss();
                // }

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                //   }
                alertDialog.show();


            }
        });

      //  arrayListUnit = (arrayListFlavour.get(position).getArrayListUnits());
        if (arrayListUnit.size() != 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityVoucherFormat.this, LinearLayoutManager.VERTICAL, false);
            rvUnit.setHasFixedSize(true);
            rvUnit.setLayoutManager(linearLayoutManager);
            RVAdapterUnitView adapterUnitView = new RVAdapterUnitView(getApplicationContext(), arrayListUnit, new DeleteListener() {
                @Override
                public void getDeleteListenerPosition(int position) {
                    arrayListUnit.remove(position);
                }
            });
            rvUnit.setAdapter(adapterUnitView);
            adapterUnitView.notifyDataSetChanged();
        }
    }

    private void init() {
        rvUnit=findViewById(R.id.rv_details);
        tvAddInfo=findViewById(R.id.btn_add);
        arrayListUnit=new ArrayList<>();
    }
}
