package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.LeaveStatusAdapter;
import com.example.marutiapp.form.LeaveStatusModel;


public class LeaveStatusActivity extends AppCompatActivity {
    Context context;
    public LeaveStatusModel[] myListData1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_leave_list);
        context=this;
        this.setTitle("Leave Status");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_LeaveListstatus);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new LeaveStatusModel[]{
                new LeaveStatusModel("", "EMP0001", "EMP1","27/04/2020","30/04/2020","Family Function","Approved"),
                new LeaveStatusModel("", "EMP0001", "EMP1","29/06/2020","30/06/2020","Family Function","Pending"),
                new LeaveStatusModel("", "EMP0001", "EMP1","27/04/2020","30/04/2020","Family Function","Approved"),
        };
        LeaveStatusAdapter adapter = new LeaveStatusAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
