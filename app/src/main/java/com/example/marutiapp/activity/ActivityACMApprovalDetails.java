package com.example.marutiapp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityACMApprovalDetails extends AppCompatActivity {

    private LinearLayout rgToggle;
    private EditText edtTotAmt, edtAmtRecieved,edtComment;
    private TextView tvApprove,tvReject;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acm_approval_detail);
        init();

        init();

        tvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityACMApprovalDetails.this);
                    dialog.setTitle("Action Confirmation");
                    dialog.setMessage("Are you sure to want to Approve?");
                    dialog.setCancelable(true);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                    dialog.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int
                                        id) {

                                    Toast.makeText(ActivityACMApprovalDetails.this, "Approved Successfully..", Toast.LENGTH_SHORT).show();
                                    // Continue with some operation
                                    dialog.dismiss();

                                }
                            });

// A null listener allows the button to dismiss the dialog and take no further action.

                    dialog.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = dialog.create();
                    alert.show();
                }


            }
        });

      /*  rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:

                        break;

                    case R.id.rd_search:
                        if(isValid()) {

                            AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityFMApprovalDetails.this);
                            dialog.setTitle("Action Confirmation");
                            dialog.setMessage("Are you sure to want to Approve?");
                            dialog.setCancelable(true);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                            dialog.setPositiveButton(
                                    "Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int
                                                id) {

                                            Toast.makeText(ActivityFMApprovalDetails.this, "Approved Successfully..", Toast.LENGTH_SHORT).show();
                                            // Continue with some operation
                                            dialog.dismiss();

                                        }
                                    });

// A null listener allows the button to dismiss the dialog and take no further action.

                            dialog.setNegativeButton(
                                    "No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = dialog.create();
                            alert.show();
                        }


                        break;
                }
            }
        });

*/
    }

    private boolean isValid() {
        if (edtTotAmt.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Enter Total Amount",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        else  if (edtAmtRecieved.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Enter Amount Recieved",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }


        return  true;
    }

    private void init() {

        rgToggle = findViewById(R.id.rgToggle);
        edtTotAmt =findViewById(R.id.edt_tot_amt);
        edtAmtRecieved =findViewById(R.id.edt_amt_recieved);
        //edtLoanAmt=findViewById(R.id.edtLoanamt);
        edtComment=findViewById(R.id.edt_comment);
        tvApprove=findViewById(R.id.tv_approve);
        tvReject=findViewById(R.id.tv_reject);
    }
}
