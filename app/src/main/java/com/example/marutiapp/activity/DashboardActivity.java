package com.example.marutiapp.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.DashboardAdapter;
import com.example.marutiapp.form.DashboardModel;

public class DashboardActivity extends AppCompatActivity {
    Context context;
    public DashboardModel[] myListData1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_dashboard);
        context = this;
        this.setTitle("Dashboard");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_dashboardcards);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new DashboardModel[]{
                new DashboardModel("", "Pending RFC", "Delivery Request Date from 01-feb-2019 to 08-feb-2020", "ACM","10"),
                new DashboardModel("", "Preliminary CheckList", "Delivery Request Date from 01-feb-2019 to 08-feb-2020", "ACM","7"),
                new DashboardModel("", "CheckList", "Delivery Request Date from 01-feb-2019 to 08-feb-2020", "ACM","6"),
        };
        DashboardAdapter adapter = new DashboardAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
