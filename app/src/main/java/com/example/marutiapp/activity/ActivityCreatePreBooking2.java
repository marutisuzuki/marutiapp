package com.example.marutiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

public class ActivityCreatePreBooking2 extends AppCompatActivity {
    Button saveandcontinue2;
    Context context;
EditText et_model_name;
Spinner sp_varientprebooking,sp_colorprebooking,sp_ScaleTypeprebooking;
String[] arrayvarient,arraycolor,arrayscaletype;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_prebooking2);
        context=this;
        init();
        initlistener();
    }

    private void initlistener() {
        saveandcontinue2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation()) {
                    Intent intent = new Intent(context, ActivityCreatePreBooking3.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean Validation() {
        if (et_model_name.getText().toString().equalsIgnoreCase("")) {
            et_model_name.setError("Enter model name");
            et_model_name.requestFocus();
            return false;
        }
        if (sp_varientprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select varient", Toast.LENGTH_SHORT).show();
        }
        if (sp_colorprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select color", Toast.LENGTH_SHORT).show();
        }
        if (sp_ScaleTypeprebooking.getSelectedItemPosition() == 0) {
            Toast.makeText(context, "Select scale type", Toast.LENGTH_SHORT).show();
        }
        return true;

    }

    private void init() {
        saveandcontinue2=findViewById(R.id.saveandcontinue2);
        et_model_name=findViewById(R.id.et_model_name);
        sp_varientprebooking=findViewById(R.id.sp_varientprebooking);
        sp_colorprebooking=findViewById(R.id.sp_colorprebooking);
        sp_ScaleTypeprebooking=findViewById(R.id.sp_ScaleTypeprebooking);

        arrayvarient = new String[]{
                "Select varient", "abx", "xyz",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayvarient);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_varientprebooking.setAdapter(adapter);

        arraycolor = new String[]{
                "Select color", "RED", "BLACK","WHITE"
        };
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraycolor);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_colorprebooking.setAdapter(adapter1);

        arrayscaletype = new String[]{
                "Select Scale type", "Abc", "xyz",
        };
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayscaletype);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_ScaleTypeprebooking.setAdapter(adapter2);
    }
}
