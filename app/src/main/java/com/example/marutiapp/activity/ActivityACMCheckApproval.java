package com.example.marutiapp.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVACMCheckListAdapter;
import com.example.marutiapp.adapter.RVACMListAdapter;
import com.example.marutiapp.form.FmListFom;

import java.util.ArrayList;

public  class ActivityACMCheckApproval extends AppCompatActivity {

    private RecyclerView rvACMChecklistApproval;

    private ArrayList<FmListFom> acmCheckListFomArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm_approval);

        init();

        //  fmListFomArrayList.clear();
        acmCheckListFomArrayList.add(new FmListFom("VIVEK U","9921088968|SOB18001944","ALTO 800 | MARUTI ALTO 800 LXI | SUPERIOR WHITE","Pending"));

      //  acmCheckListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
        //  fmListFomArrayList.add(new FmListFom("M/S PUSHKRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
        //acmListFomArrayList.add(new FmListFom("M/S RAVIKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Approved"));
       // acmListFomArrayList.add(new FmListFom("M/S YASHRAJ TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));
       // acmListFomArrayList.add(new FmListFom("M/S ROOPESH TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        //     fmListFomArrayList.add(new FmListFom("M/S RAJKUMAR TOURS AND TRAVELERS","9921088210|SOB18001330","NEW SWIFT DZIRE  |MARUTI TOURS CNG | PEARL ARCTIC WHITE","Pending"));

        if (acmCheckListFomArrayList != null && acmCheckListFomArrayList.size() > 0) {
            rvACMChecklistApproval.setVisibility(View.GONE);
            rvACMChecklistApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvACMChecklistApproval.setLayoutManager(linearLayoutManager);
            RVACMCheckListAdapter acmChecklist = new RVACMCheckListAdapter(ActivityACMCheckApproval.this, acmCheckListFomArrayList);

            rvACMChecklistApproval.setHasFixedSize(true);
            rvACMChecklistApproval.setNestedScrollingEnabled(false);

            rvACMChecklistApproval.setAdapter(acmChecklist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }

    }

    private void init() {
        rvACMChecklistApproval =findViewById(R.id.rv_fm_list);
        acmCheckListFomArrayList =new ArrayList<>();

    }
}


