package com.example.marutiapp.activity;

import android.os.Bundle;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVToolsDetailsAdapter;
import com.example.marutiapp.form.ToolsForm;

import java.util.ArrayList;

public class ActivityDeliveryMemo extends AppCompatActivity {
    private RecyclerView rvToolsDetails;
    private ArrayList<ToolsForm>  toolsFormArrayList;
    private EditText edtCustName,edtCarName,edtAllotmentNo,edtAllotmentDate,edtChassisNo,edtEngineNo,edtColour,
    edtRegNo,edtInsuranceNo,edtInvNo,edtcustSign,edtDeliveryBy,edtAuthorisedBy,edtUnderName,edtUnderChasisse,edtUnderEng,edtUnderDate,edtUnderCustSign,edtUnderExecutive;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_memo);
        init();
        toolsFormArrayList.clear();
        toolsFormArrayList.add(new ToolsForm("Jack"));
        toolsFormArrayList.add(new ToolsForm("Jack Handle"));
        toolsFormArrayList.add(new ToolsForm("Owners Manual"));
        toolsFormArrayList.add(new ToolsForm("Carpet"));
        toolsFormArrayList.add(new ToolsForm("Outside Mirror"));

        if (toolsFormArrayList != null && toolsFormArrayList.size() > 0) {
         //   rvRTOCheckApproval.setVisibility(View.GONE);
        //    rvRTOCheckApproval.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvToolsDetails.setLayoutManager(linearLayoutManager);
            RVToolsDetailsAdapter rtoChecklist = new RVToolsDetailsAdapter(ActivityDeliveryMemo.this, toolsFormArrayList);

            rvToolsDetails.setHasFixedSize(true);
            rvToolsDetails.setNestedScrollingEnabled(false);

            rvToolsDetails.setAdapter(rtoChecklist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        rvToolsDetails=findViewById(R.id.rv_tools_details);
        toolsFormArrayList=new ArrayList<>();
        edtCustName= findViewById(R.id.edt_cust_name);
        edtCarName=findViewById(R.id.edt_car_name);
        edtAllotmentNo=findViewById(R.id.edt_allotment_no);
        edtAllotmentDate=findViewById(R.id.edt_allotment_date);
        edtChassisNo=findViewById(R.id.edt_chassis_no);
        edtEngineNo=findViewById(R.id.edt_engine_no);
        edtColour=findViewById(R.id.edt_color);
        edtRegNo=findViewById(R.id.edt_registration_no);
        edtInsuranceNo=findViewById(R.id.edt_insurance_no);
        edtcustSign=findViewById(R.id.edt_cust_sign);
        edtDeliveryBy=findViewById(R.id.edt_delivery_by);
        edtAuthorisedBy=findViewById(R.id.edt_authorised_sign);
        edtUnderName=findViewById(R.id.edt_under_name);
        edtUnderChasisse=findViewById(R.id.edt_under_chasiss_no);
        edtUnderEng=findViewById(R.id.edt_under_engine_no);
        edtUnderDate=findViewById(R.id.edt_under_date);
        edtUnderCustSign=findViewById(R.id.edt_under_customer_sign);



    }
}
