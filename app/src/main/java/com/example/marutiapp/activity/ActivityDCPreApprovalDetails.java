package com.example.marutiapp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.marutiapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActivityDCPreApprovalDetails extends AppCompatActivity {

    private TextView tvSubmit,tvSave;
    EditText edtComment,edtSelectDate;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    private  Calendar mcurrentDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc_approval_detail);
        init();

        edtSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(ActivityDCPreApprovalDetails.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        // String myFormat = "yyyy-MM-dd h:mm:ss a";
                        String myFormat = "dd-MM-yyyy"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                       String     getStartDate = sdf.format(myCalendar.getTime());
                        edtSelectDate.setText(getStartDate);
                    }
                }, mYear, mMonth, mDay);
              /*  mDatePicker.setTitle("Select date");

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String formattedDate = df.format(c);
                try {
                    Date d = df.parse(formattedDate);
                    mDatePicker.getDatePicker().setMinDate(d.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }*/
                mDatePicker.show();

            }
        });

       /* tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent=new Intent(ActivityDCApprovalDetails.this,ActivityALMApproved.class);
               // startActivity(intent);
            }
        });
*/

       tvSubmit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(isValid())
               {
                   Toast.makeText(ActivityDCPreApprovalDetails.this, "Submitted Successfully", Toast.LENGTH_SHORT).show();

               }

           }
       });

       tvSave.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(isValid())
               {
                   Toast.makeText(ActivityDCPreApprovalDetails.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
               }
           }
       });



    }

    private boolean isValid() {
          if (edtComment.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Enter Comment",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        else  if (edtSelectDate.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this,"Select  Date",Toast.LENGTH_SHORT).show();
            // et_emailIDprebooking.setError("Enter EmailID");
            // et_emailIDprebooking.requestFocus();
            return false;
        }
        return true;

    }

    private void init() {
        edtComment=findViewById(R.id.edt_comment);
        edtSelectDate=findViewById(R.id.edt_date);
        tvSubmit=findViewById(R.id.tv_submit);
        tvSave=findViewById(R.id.tv_save);
        //tvSubmit = findViewById(R.id.tv_submit);
    }
}




