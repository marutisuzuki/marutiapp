package com.example.marutiapp.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marutiapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentChangePassword#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentChangePassword extends Fragment {
  private Button btn_changepass;
   private TextView et_confirmedpass,et_newpass,et_oldpass;
   private  View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentChangePassword() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentChangePassword.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChangePassword newInstance() {
        FragmentChangePassword fragment = new FragmentChangePassword();
        Bundle args = new Bundle();
       // args.putString(ARG_PARAM1, param1);
     //   args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_changepassword, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        initlistener();

    }


    private void initlistener() {
        btn_changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validation()){
                    Toast.makeText(getActivity(),"Password Changed Successfully",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean validation() {
        if(et_oldpass.getText().toString().equalsIgnoreCase("")){
            et_oldpass.setError("Enter Old Password");
            return false;

        }
        if(et_newpass.getText().toString().equalsIgnoreCase("")){
            et_newpass.setError("Enter New Password");
            return false;

        }
        if(et_confirmedpass.getText().toString().equalsIgnoreCase("")){
            et_confirmedpass.setError("Enter Confirmed Password");
            return false;

        }


        return true;
    }

    private void init() {
        et_oldpass=view.findViewById(R.id.et_oldpass);
        et_newpass=view.findViewById(R.id.et_newpass);
        et_confirmedpass=view.findViewById(R.id.et_confirmedpass);
        btn_changepass=view.findViewById(R.id.btn_changepass);
    }
}
