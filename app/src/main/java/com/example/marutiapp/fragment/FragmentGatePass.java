package com.example.marutiapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityDeliveryMemo;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentGatePass#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGatePass extends Fragment {
    private Spinner spExecutive;
    private RadioGroup rgToggle;
    private View view;
    private EditText edtCarName, edtCarNo, edtInvNo, edtInvDate, edtGatePassDate, edtCustId, edtCustName, edtCustAdd, edtModelName, edtVehId,
            edtRegId, edtEngineNo, edtChassisNo, edtColour;


    // TODO: Rename and change types and number of parameters
    public static FragmentGatePass newInstance(String param1, String param2) {
        FragmentGatePass fragment = new FragmentGatePass();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_gate_pass, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        spExecutive.setPrompt("Select Sales Executive");

        rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_pending:
                       /* llApplyLeave.setVisibility(View.VISIBLE);
                        flLeaveHistory.setVisibility(View.GONE);*/
                        break;

                    case R.id.rd_checkout:
                        if (isValid()) {

                            Intent intent = new Intent(getActivity(), ActivityDeliveryMemo.class);
                            startActivity(intent);
                        }
                      /*  llApplyLeave.setVisibility(View.GONE);
                        flLeaveHistory.setVisibility(View.VISIBLE);
                        getLeaveHistory();*/
                        break;
                }
            }

            private boolean isValid() {
                if (edtCarName.getText().toString().equalsIgnoreCase("")) {
                    edtCarName.setError("Enter Car Name");
                    return false;
                } else if (edtCarNo.getText().toString().equalsIgnoreCase("")) {
                    edtCarNo.setError("Enter Car No");
                    return false;
                } else if (edtInvNo.getText().toString().equalsIgnoreCase("")) {
                    edtInvNo.setError("Enter Invoice No");
                    return false;
                } else if (edtInvDate.getText().toString().equalsIgnoreCase("")) {
                    edtInvDate.setError("Enter Invoice Date");
                    return false;
                } else if (edtGatePassDate.getText().toString().equalsIgnoreCase("")) {
                    edtGatePassDate.setError("Enter Gate Pass Date");
                    return false;
                } else if (edtCustId.getText().toString().equalsIgnoreCase("")) {
                    edtCustId.setError("Enter Customer Id");
                    return false;
                } else if (edtCustName.getText().toString().equalsIgnoreCase("")) {
                    edtCustName.setError("Enter Customer Name");
                    return false;
                } else if (edtCustAdd.getText().toString().equalsIgnoreCase("")) {
                    edtCustAdd.setError("Enter Customer Address");
                    return false;
                } else if (edtRegId.getText().toString().equalsIgnoreCase("")) {
                    edtRegId.setError("Enter Registration No");
                    return false;
                } else if (edtVehId.getText().toString().equalsIgnoreCase("")) {
                    edtVehId.setError("Enter Vehicle Id");
                    return false;
                } else if (edtEngineNo.getText().toString().equalsIgnoreCase("")) {
                    edtEngineNo.setError("Enter Engine No");
                    return false;
                } else if (edtChassisNo.getText().toString().equalsIgnoreCase("")) {
                    edtChassisNo.setError("Enter Chassis No");
                    return false;

                } else if (edtModelName.getText().toString().equalsIgnoreCase("")) {
                    edtModelName.setError("Enter Model Name");
                    return false;

                } else if (edtColour.getText().toString().equalsIgnoreCase("")) {
                    edtColour.setError("Enter Color");
                    return false;
                }
                return true;
            }
        });
    }


    private void init() {
        spExecutive = view.findViewById(R.id.sp_executive);
        rgToggle = view.findViewById(R.id.rgToggle);
        edtCarName = view.findViewById(R.id.edt_car_name);
        edtCarNo = view.findViewById(R.id.edt_car_no);
        edtInvNo = view.findViewById(R.id.edt_einv_no);
        edtInvDate = view.findViewById(R.id.edt_inv_date);
        edtGatePassDate = view.findViewById(R.id.edt_gate_pass_date);
        edtCustId = view.findViewById(R.id.edt_cust_id);
        edtCustName = view.findViewById(R.id.edt_cust_name);
        edtCustAdd = view.findViewById(R.id.edt_address);
        edtRegId = view.findViewById(R.id.edt_reg_no);
        edtVehId = view.findViewById(R.id.edt_veh_id);
        edtEngineNo = view.findViewById(R.id.edt_engine_no);
        edtChassisNo = view.findViewById(R.id.edt_chassis_no);
        edtModelName = view.findViewById(R.id.edt_model_name);
        edtColour = view.findViewById(R.id.edt_color);
    }
}
