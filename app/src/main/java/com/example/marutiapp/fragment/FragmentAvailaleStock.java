package com.example.marutiapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.AvailableStockAdapter;
import com.example.marutiapp.form.AvailableStockModel;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentAvailaleStock#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAvailaleStock extends Fragment {
    private  View view;
    private Context context;
    public AvailableStockModel[] myListData1;
    private  String[] arraySpinner,arrayvarient,arraycolor;
   private RecyclerView recyclerView;
   private LinearLayoutManager layoutManager;
   private Spinner sp_color,sp_model,sp_varient;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentAvailaleStock() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAvailaleStock.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAvailaleStock newInstance(String param1, String param2) {
        FragmentAvailaleStock fragment = new FragmentAvailaleStock();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_available_stock, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        myListData1 = new AvailableStockModel[]{
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "12","1111"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "13","233"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "14","142"),
                new AvailableStockModel("", "ALTO |K10 VXI |SUPER", "15","123"),
        };

        AvailableStockAdapter adapter = new AvailableStockAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void init() {
        recyclerView = (RecyclerView)view. findViewById(R.id.rv_availableStock);
        sp_color =view. findViewById(R.id.sp_color);
        sp_model =view. findViewById(R.id.sp_model);
        sp_varient =view. findViewById(R.id.sp_varient);
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        arraySpinner = new String[]{
                "Select Model", "abc", "abc1",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_model.setAdapter(adapter);
        arrayvarient = new String[]{
                "Select Varient", "abc", "abc1",
        };
        ArrayAdapter<String> adaptervarient = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arrayvarient);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_varient.setAdapter(adaptervarient);


        arraycolor = new String[]{
                "Select Color", "BLUE", "RED","GREEN",
        };
        ArrayAdapter<String> adaptercolor = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraycolor);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_color.setAdapter(adaptercolor);
    }
}