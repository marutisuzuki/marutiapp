package com.example.marutiapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.LeaveListAdapter;
import com.example.marutiapp.form.LeaveListModel;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentLeaveApplication#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentLeaveApplication extends Fragment {
    private  View view;
    Context context;
    public LeaveListModel[] myListData1;
    TextView deliveryReqdate, tv_empName, tv_empFromdate, tv_empTodate;
    Button btn_initiateRFC, btn_save, btn_viewApproval,btn_attachletter;
    EditText et_reason;
    RecyclerView recyclerView;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentLeaveApplication() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentLeaveApplication.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentLeaveApplication newInstance(String param1, String param2) {
        FragmentLeaveApplication fragment = new FragmentLeaveApplication();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_apply_leave_emp, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myListData1 = new LeaveListModel[]{
                new LeaveListModel("", "23/04/2020", "", ""),
                new LeaveListModel("", "27/07/2020", "", ""),
        };
        LeaveListAdapter adapter = new LeaveListAdapter(myListData1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void init() {
        recyclerView =view. findViewById(R.id.rv_leaveList);
        deliveryReqdate =view. findViewById(R.id.deliveryReqdate);
        tv_empName =view. findViewById(R.id.tv_empName);
        tv_empFromdate =view. findViewById(R.id.tv_empFromdate);
        tv_empTodate =view. findViewById(R.id.tv_empTodate);
        btn_initiateRFC =view. findViewById(R.id.btn_initiateRFC);
        btn_save =view. findViewById(R.id.btn_save);
        et_reason =view. findViewById(R.id.et_reason);
        btn_viewApproval =view. findViewById(R.id.btn_viewApproval);
        btn_attachletter =view. findViewById(R.id.btn_attachletter);
    }
}
