package com.example.marutiapp.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RadioGroup;
import android.widget.TextView;


import com.example.marutiapp.DatePickerDialog;
import com.example.marutiapp.R;
import com.example.marutiapp.activity.ActivityACMApproval;
import com.example.marutiapp.activity.ActivityFmApproval;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentACM#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentACM extends Fragment implements DatePickerDialog.OnDateSetListener{
    private  View view;
    private RecyclerView rvFm;
    private RadioGroup rgToggle;
    private boolean mAutoHighlight;
    private TextView tvDate;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentACM() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentACM.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentACM newInstance(String param1, String param2) {
        FragmentACM fragment = new FragmentACM();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_fm, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        init();

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        (DatePickerDialog.OnDateSetListener) getActivity(),
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            }
        });

        rgToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rd_reset:
                       /* llApplyLeave.setVisibility(View.VISIBLE);
                        flLeaveHistory.setVisibility(View.GONE);*/
                        break;

                    case R.id.rd_search:




                        Intent intent=new Intent(getActivity(), ActivityACMApproval.class);
                        startActivity(intent);

                      /*  llApplyLeave.setVisibility(View.GONE);
                        flLeaveHistory.setVisibility(View.VISIBLE);
                        getLeaveHistory();*/
                        break;
                }
            }
        });
    }

    private void init() {
        // rvFm=findViewById(R.id.rv_fm);
        rgToggle = view.findViewById(R.id.rgToggle);
        tvDate=view.findViewById(R.id.tv_date);
    }

    /*@Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "You picked the following date: From- "+dayOfMonth+"/"+(++monthOfYear)+"/"+year+" To "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        //dateTextView.setText(date);
    }*/



    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

    }
}



