package com.example.marutiapp.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.DisplayExcelListAdapter;
import com.example.marutiapp.form.ExcelListModel;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentUploadStockInfo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentUploadStockInfo extends Fragment {
    private  View view;
   private TextView tv_viewdetails,tv_excelformat1,tv_excelformat2,tv_excelformat3;
  private   Button btn_initiateRFC,btn_uploadimg,btn_logout,btn_uploadexcelCancel,btn_uploadexcelSubmit;
private     boolean doubleBackToExitPressedOnce = false;
     public ExcelListModel[] myListData1;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentUploadStockInfo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentUploadStockInfo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentUploadStockInfo newInstance(String param1, String param2) {
        FragmentUploadStockInfo fragment = new FragmentUploadStockInfo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_upload_stock, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        initlistener();
       // getdata();
    }

    /*private void getdata() {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedfile = data.getData(); //The uri with the location of the file
        }
    }*/
    private void initlistener() {
        tv_viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent  intent=new Intent(ActivityUploadStock.this,ExcelListActivity.class);
                startActivity(intent);*/
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.activity_recycler_excel_list);
                myListData1 = new ExcelListModel[]{
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample1.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample2.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample3.xls"),
                        new ExcelListModel("", R.drawable.stockuploadimg1, "Sample4.xls"),
                };
                RecyclerView recyclerView = dialog.findViewById(R.id.rv_excel_list);
                ImageView img_cancel=dialog.findViewById(R.id.img_cancel);
                DisplayExcelListAdapter adapter = new DisplayExcelListAdapter(myListData1);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(adapter);
                img_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_uploadimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
            }
        });
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("Logout");
                dialog.setMessage("Are you sure you want to logout?");
                dialog.setCancelable(true);
                dialog.setIcon(R.drawable.ic_power_settings_new_black_24dp);

// Specifying a listener allows you to take an action before dismissing the dialog.
// The dialog is automatically dismissed when a dialog button is clicked.

                dialog.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int
                                    id) {

                                Toast.makeText(getActivity(), "Logout successfully", Toast.LENGTH_SHORT).show();
                                // Continue with some operation
                                dialog.dismiss();

                            }
                        });

// A null listener allows the button to dismiss the dialog and take no further action.

                dialog.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = dialog.create();
                alert.show();

            }
        });
    }

    private void init() {
        btn_uploadexcelCancel=view.findViewById(R.id.btn_uploadexcelCancel);
        btn_uploadexcelSubmit=view.findViewById(R.id.btn_uploadexcelSubmit);
        tv_excelformat1=view.findViewById(R.id.tv_excelformat1);
        tv_excelformat2=view.findViewById(R.id.tv_excelformat2);
        tv_excelformat3=view.findViewById(R.id.tv_excelformat3);
        tv_viewdetails=view.findViewById(R.id.tv_viewdetails);
        btn_uploadimg=view.findViewById(R.id.btn_uploadimg);
        btn_logout=view.findViewById(R.id.btn_logout);
    }


}



