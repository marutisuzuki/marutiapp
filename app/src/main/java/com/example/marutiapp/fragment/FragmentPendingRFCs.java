package com.example.marutiapp.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.ViewPagerAdapterCheckList;
import com.example.marutiapp.adapter.ViewPagerAdapterPendingRFCs;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentPendingRFCs#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPendingRFCs extends Fragment {
    private  View view;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentPendingRFCs() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPendingRFCs.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPendingRFCs newInstance(String param1, String param2) {
        FragmentPendingRFCs fragment = new FragmentPendingRFCs();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_pending_rfcs, container, false);
        return  view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager mViewPager) {
        FragmentManager cfManager = getChildFragmentManager();
        ViewPagerAdapterPendingRFCs adapter = new ViewPagerAdapterPendingRFCs(cfManager);
       // ViewPagerAdapterPendingRFCs adapter = new ViewPagerAdapterPendingRFCs(getActivity().getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
    }

    private void init() {
      //  mToolbar = (Toolbar)view. findViewById(R.id.toolbar);
        mTabLayout = (TabLayout) view.findViewById(R.id.tabs_rfcs);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager_rfcs);
    }
}
