package com.example.marutiapp.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.marutiapp.R;
import com.example.marutiapp.adapter.RVAccAdapter;
import com.example.marutiapp.form.AccesseriosForm;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentPriceDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPriceDetails extends Fragment {
    private  View view;
    private RecyclerView rvAccDetails;
    private ArrayList<AccesseriosForm> accesseriosFormArrayList;
    private EditText edtCustName,edtVehModel,edtVehColor,edtShowRoomPrice,edtMarutiInsurance,edtRoadTax,edtWarenty,edtOnRoadPrice,edtConsumerOffer,edtCorporateOffer;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentPriceDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPriceDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPriceDetails newInstance(String param1, String param2) {
        FragmentPriceDetails fragment = new FragmentPriceDetails();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_price_details, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        accesseriosFormArrayList.add(new AccesseriosForm("Car Covers","1","100"));
        accesseriosFormArrayList.add(new AccesseriosForm("Booster Cable","1","200"));
        accesseriosFormArrayList.add(new AccesseriosForm("Car Mats","1","50"));
        accesseriosFormArrayList.add(new AccesseriosForm("Seat Covers","1","70"));
        accesseriosFormArrayList.add(new AccesseriosForm("Cleaning Equipment","1","100"));
        if (accesseriosFormArrayList != null && accesseriosFormArrayList.size() > 0) {
            rvAccDetails.setVisibility(View.GONE);
            rvAccDetails.setVisibility(View.VISIBLE);
            //            rvVehicleList.setLayoutManager(linearLayoutManager);
            //  rvVehicleList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvAccDetails.setLayoutManager(linearLayoutManager);
            RVAccAdapter acclist = new RVAccAdapter(getActivity(), accesseriosFormArrayList);

            rvAccDetails.setHasFixedSize(true);
            rvAccDetails.setNestedScrollingEnabled(false);

            rvAccDetails.setAdapter(acclist);
        } else {
            // llUpComingNoData.setVisibility(View.GONE);
            // llPostNoData.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        rvAccDetails=view.findViewById(R.id.rv_acc_details);
        accesseriosFormArrayList=new ArrayList<>();
        edtCustName=view.findViewById(R.id.edt_cust_name);
        edtVehModel=view.findViewById(R.id.edt_model_name);
        edtVehColor=view.findViewById(R.id.edt_color);
        edtShowRoomPrice=view.findViewById(R.id.edt_show_room_price);
        edtMarutiInsurance=view.findViewById(R.id.edt_maruti_insurance);
        edtRoadTax=view.findViewById(R.id.edt_road_tax);
        edtWarenty=view.findViewById(R.id.edt_warenty);
        edtOnRoadPrice=view.findViewById(R.id.edt_road_price);
        edtConsumerOffer=view.findViewById(R.id.edt_consumer_offer);
        edtCorporateOffer=view.findViewById(R.id.edt_corporate_offer);
    }
}
